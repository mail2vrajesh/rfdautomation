package com.ril.qa;

import java.util.HashMap;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ril.lib.ReadExcel;
import com.ril.lib.TestsBase;
import com.ril.lib.Validator;
import com.ril.lib.utils;

/**
 * LoginTest test the sign in functionality of the web site
 * 
 * @author Shankar.Kumar
 *
 */
public class LoginTest extends TestsBase {
	
	/*@BeforeClass
	public void test(){
		System.out.println("Shankar");
	}*/

	@DataProvider(name = "SignInTestdata")
	public static Object[][] testData() throws Exception {
		ReadExcel re = new ReadExcel(utils.getConfig("DataSheet"), "SignIn");
		return re.getTableToHashMapDoubleArray();
	}

	@Test(dataProvider = "SignInTestdata")
	public void testLogin(HashMap<String, String> hm) throws Exception {
		if (hm.get("ExecuteFlag").trim().equalsIgnoreCase("No"))
			throw new SkipException(
					"Skipping the test ---->> As per excel entry");
		logger.info("Test execution for: test case number>> " + hm.get("Sno")
				+ " **** " + hm.get("TestDescription"));
		driver.get(utils.getConfig("Url"));
		// maximize the window
		driver.manage().window().maximize();
		isHomePageLoaded();
		click(getLocator("Home", "signInLink"), 5);
		performSignIn(hm.get("I_LoginID"), hm.get("I_Pwd"));
		if (hm.get("IsNegativeCase").equalsIgnoreCase("Yes")) {
			Assert.assertTrue(
					hm.get("O_ErrorMessage").equals(
							getText(getLocator("SignIn", "ErrorMsg"), 5)),
					"Error message has been shown successfully to the user");
		} else {
			Assert.assertTrue(
					Validator.isElementPresent(driver,
							getLocator("Home", "signOutLink"), 10),
					"Valid user couldn't login");
		}

	}

	public void isHomePageLoaded() {
		// Check if the page is loaded
		Assert.assertTrue(
				driver.getTitle().equalsIgnoreCase(
						utils.getConfig("HomePageTitle")),
				"The page title doesn't matched. Page" + "didn't load");
	}

	public void performSignIn(String userName, String password)
			throws Exception {
		setValue(getLocator("SignIn", "LoginIDTxt"), userName, 10);
		setValue(getLocator("SignIn", "Password"), password, 10);
		click(getLocator("SignIn", "SignInBtn"), 5);
	}

}
