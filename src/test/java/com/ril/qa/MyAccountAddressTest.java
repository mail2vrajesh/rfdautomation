package com.ril.qa;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ril.lib.ReadExcel;
import com.ril.lib.TestsBase;
import com.ril.lib.Validator;
import com.ril.lib.utils;

/**
 * 
 * @author Shankar.Kumar
 *
 */
public class MyAccountAddressTest extends TestsBase {

	boolean addNewAddressFlag = false;

	@DataProvider(name = "MyAccountAddressData")
	public static Object[][] testData() throws Exception {
		ReadExcel re = new ReadExcel(utils.getConfig("DataSheet"), "MyAddress");
		return re.getTableToHashMapDoubleArray();
	}

	@Test(dataProvider = "MyAccountAddressData")
	public void myAccountTest(HashMap<String, String> hm) throws Exception {

		try {
			if (hm.get("ExecuteFlag").trim().equalsIgnoreCase("N")) {
				throw new SkipException(
						"The test case has been skipped in test data skip flag");
			}
			driver.get(utils.getConfig("Url"));
			// maximize the window
			driver.manage().window().maximize();
			click(getLocator("Home", "signInLink"), 5);
			performSignIn(hm.get("UserName"), hm.get("Password"));
			click(getLocator("Home", "myAccountLink"), 10);
			/*
			 * After successful login perform different checks
			 */
			Validator.isTextPresent(driver, "My Account");

			switch (hm.get("FunctionalityCheck")) {

			case "ADD_NEW_ADDRESS":
				addNewAddressFlag = true;
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				click(getLocator("MyAccount", "myAddBookLink"), 10);
				click(getLocator("AddressBook", "addNewAddress"), 10);
				fillAddressForm(hm.get("I_Pincode"), hm.get("I_Locality"),
						hm.get("I_AddressName"), hm.get("I_Fname"),
						hm.get("I_Lname"), hm.get("I_FlatNo"),
						hm.get("I_Society"), hm.get("I_Address1"),
						hm.get("I_Address2"));
				click(getLocator("AddressBook", "submitBtn"), 10);
				Thread.sleep(5000);
				if (!driver
						.findElement(By.tagName("html"))
						.getText()
						.contains(
								"The new address has been successfully added to the address book.")) {
					Assert.fail("verifyTextPresent failed");
				}
				driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
				break;
			case "VERIFY_NEW_ADDRESS":
				if (!addNewAddressFlag) {
					Assert.fail("Verify new address scenario is dependent on add address scenario");
				}
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				click(getLocator("MyAccount", "myAddBookLink"), 10);
				selectAddress(hm.get("I_AddressName"));
				verifyAddress(hm.get("I_AddressName"), hm.get("I_Fname"),
						hm.get("I_Lname"), hm.get("I_FlatNo"),
						hm.get("I_Society"), hm.get("I_Address1"),
						hm.get("I_Address2"), hm.get("I_Pincode"),
						hm.get("I_Locality"), hm.get("I_City"),
						hm.get("I_State"));
				driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
				break;
			case "VERIFY_DUPLICATE_ADDRESS":
				// Duplicate address scenario is dependent on add address
				// scenario
				if (!addNewAddressFlag) {
					Assert.fail("Duplicate address scenario is dependent on add address scenario");
				}
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				click(getLocator("MyAccount", "myAddBookLink"), 10);
				try {
					selectAddress(hm.get("I_AddressName"));
				} catch (Exception e) {
					Assert.fail("Select address has failed");
				}
				click(getLocator("AddressBook", "addNewAddress"), 10);
				fillAddressForm(hm.get("I_Pincode"), hm.get("I_Locality"),
						hm.get("I_AddressName"), hm.get("I_Fname"),
						hm.get("I_Lname"), hm.get("I_FlatNo"),
						hm.get("I_Society"), hm.get("I_Address1"),
						hm.get("I_Address2"));
				click(getLocator("AddressBook", "submitBtn"), 10);
				Thread.sleep(5000);
				if (!driver
						.findElement(By.tagName("html"))
						.getText()
						.contains(
								"The address name entered already exists. Type another name in the address name field and try again.")) {
					Assert.fail("verifyTextPresent failed");
				}
				driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
				break;
			case "REMOVE_EXISTING_ADDRESS":
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				click(getLocator("MyAccount", "myAddBookLink"), 10);
				try {
					selectAddress(hm.get("I_AddressName"));
				} catch (Exception e) {
					Assert.fail("Select address has failed");
				}
				click(getLocator("AddressBook", "removeBtn"), 10);
				try {
					selectAddress(hm.get("I_AddressName"));
				} catch (Exception e) {
					Assert.assertTrue(true,
							"Drop down value is not available as expected: "
									+ hm.get("I_AddressName"));
				}
				driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
				break;

			case "VERIFY_MANDATORY_ADDRESS_FIELD":
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				click(getLocator("MyAccount", "myAddBookLink"), 10);
				click(getLocator("AddressBook", "addNewAddress"), 10);
				fillAddressForm(hm.get("I_Pincode"), hm.get("I_Locality"),
						hm.get("I_AddressName"), hm.get("I_Fname"),
						hm.get("I_Lname"), hm.get("I_FlatNo"),
						hm.get("I_Society"), hm.get("I_Address1"),
						hm.get("I_Address2"));
				click(getLocator("AddressBook", "submitBtn"), 10);
				Validator.verifyTextForElement(driver,
						getLocator("AddressBook", "tooltipError"),
						hm.get("O_ErrorMessage"), 5);
				break;
			default:
				break;
			}

		} catch (SkipException se) {
			logger.info("Test case skipped in skip flag of test data");

		}

	}

	public void performSignIn(String userName, String password)
			throws Exception {
		setValue(getLocator("SignIn", "LoginIDTxt"), userName, 20);
		setValue(getLocator("SignIn", "Password"), password, 20);
		click(getLocator("SignIn", "SignInBtn"), 10);
	}

	public void fillAddressForm(String... addDetail) throws Exception {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// Check number of arguments
		int argLen = addDetail.length;
		if (argLen == 0) {
			// Message to print
			return;
		} else if (argLen > 9) {
			// Message to print
			return;
		}
		try {
			for (int i = 0; i < addDetail.length; i++) {
				switch (i) {
				case 0:
					if (!addDetail[i].trim().equals("")) {
						Thread.sleep(5000);
						setValue(getLocator("AddressBook", "pincode"),
								addDetail[i], 10);
					}
					break;
				case 1:
					if (!addDetail[i].trim().equals("")) {
						Thread.sleep(5000);
						selectValueinDropDown(
								getLocator("AddressBook", "locationDrpDown"),
								addDetail[i], 10);
						Thread.sleep(5000);
					}
					break;
				case 2:
					if (!addDetail[i].trim().equals("")) {
						setValue(getLocator("AddressBook", "addName"),
								addDetail[i], 10);
					}
					break;
				case 3:
					if (!addDetail[i].trim().equals("")) {
						setValue(getLocator("AddressBook", "firstName"),
								addDetail[i], 10);
					}
					break;
				case 4:
					if (!addDetail[i].trim().equals("")) {
						setValue(getLocator("AddressBook", "lastName"),
								addDetail[i], 10);
					}
					break;
				case 5:
					if (!addDetail[i].trim().equals("")) {
						setValue(getLocator("AddressBook", "flatNo"),
								addDetail[i], 10);
					}
					break;
				case 6:
					if (!addDetail[i].trim().equals("")) {
						setValue(getLocator("AddressBook", "buildingNo"),
								addDetail[i], 10);
					}
					break;
				case 7:
					if (!addDetail[i].trim().equals("")) {
						setValue(getLocator("AddressBook", "addLine1"),
								addDetail[i], 10);
					}
					break;
				case 8:
					if (!addDetail[i].trim().equals("")) {
						setValue(getLocator("AddressBook", "addLine2"),
								addDetail[i], 10);
					}
					break;

				default:
					break;
				}
			}

		} catch (ArrayIndexOutOfBoundsException aiobex) {
			driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
			return;
		} finally {
			driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		}
	}

	public boolean verifyAddress(String... addDetail) throws Exception {
		boolean condition = false;
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// Check number of arguments
		int argLen = addDetail.length;
		if (argLen == 0) {
			// Message to print
			return false;
		} else if (argLen > 9) {
			// Message to print
			return false;
		}
		try {
			for (int i = 0; i < addDetail.length; i++) {
				switch (i) {
				case 0:
					if (!addDetail[i].trim().equals("")) {
						condition = driver
								.findElement(
										getLocator("AddressBook", "addName"))
								.getAttribute("value").trim()
								.equalsIgnoreCase(addDetail[i]);
						Assert.assertTrue(condition,
								"Update of address name is not true");
					}
					break;
				case 1:
					if (!addDetail[i].trim().equals("")) {
						condition = driver
								.findElement(
										getLocator("AddressBook", "firstName"))
								.getAttribute("value").trim()
								.equalsIgnoreCase(addDetail[i]);
						Assert.assertTrue(condition,
								"Update of first name is not true");
					}
					break;
				case 2:
					if (!addDetail[i].trim().equals("")) {
						condition = driver
								.findElement(
										getLocator("AddressBook", "lastName"))
								.getAttribute("value").trim()
								.equalsIgnoreCase(addDetail[i]);
						Assert.assertTrue(condition,
								"Update of last name is not true");
					}
					break;
				case 3:
					if (!addDetail[i].trim().equals("")) {
						condition = driver
								.findElement(
										getLocator("AddressBook", "flatNo"))
								.getAttribute("value").trim()
								.equalsIgnoreCase(addDetail[i]);
						Assert.assertTrue(condition,
								"Update of flat number or wing or block is not true");
					}
					break;
				case 4:
					if (!addDetail[i].trim().equals("")) {
						condition = driver
								.findElement(
										getLocator("AddressBook", "buildingNo"))
								.getAttribute("value").trim()
								.equalsIgnoreCase(addDetail[i]);
						Assert.assertTrue(condition,
								"Update of building number or society is not true");
					}
					break;
				case 5:
					if (!addDetail[i].trim().equals("")) {
						condition = driver
								.findElement(
										getLocator("AddressBook", "addLine1"))
								.getAttribute("value").trim()
								.equalsIgnoreCase(addDetail[i]);
						Assert.assertTrue(condition,
								"Update of address line 1 is not true");
					}
					break;
				case 6:
					if (!addDetail[i].trim().equals("")) {
						condition = driver
								.findElement(
										getLocator("AddressBook", "addLine2"))
								.getAttribute("value").trim()
								.equalsIgnoreCase(addDetail[i]);
						Assert.assertTrue(condition,
								"Update of address line 2 is not true");
					}
					break;
				case 7:
					if (!addDetail[i].trim().equals("")) {
						condition = driver
								.findElement(
										getLocator("AddressBook", "pincode"))
								.getAttribute("value").trim()
								.equalsIgnoreCase(addDetail[i]);
						Assert.assertTrue(condition,
								"Update of pincode is not true");
					}
					break;
				case 8:
					if (!addDetail[i].trim().equals("")) {
						condition = driver
								.findElement(
										getLocator("AddressBook",
												"locationDrpDown"))
								.getAttribute("value").trim()
								.equalsIgnoreCase(addDetail[i]);
						Assert.assertTrue(condition,
								"Update of locality name is not true");
					}
					break;
				case 9:
					if (!addDetail[i].trim().equals("")) {
						condition = driver
								.findElement(getLocator("AddressBook", "city"))
								.getAttribute("value").trim()
								.equalsIgnoreCase(addDetail[i]);
						Assert.assertTrue(condition,
								"Update of city name is not true");
					}
					break;
				case 10:
					if (!addDetail[i].trim().equals("")) {
						condition = driver
								.findElement(getLocator("AddressBook", "state"))
								.getAttribute("value").trim()
								.equalsIgnoreCase(addDetail[i]);
						Assert.assertTrue(condition,
								"Update of state name is not true");
					}
					break;

				default:
					break;
				}
			}

		} catch (ArrayIndexOutOfBoundsException aiobex) {
			driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
			return condition;
		} finally {
			driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		}
		return condition;
	}

	public void selectAddress(String AddressName) throws Exception {
		selectValueinDropDown(getLocator("AddressBook", "addressDropDown"),
				AddressName, 10);
	}
}
