package com.ril.qa;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ril.lib.ReadExcel;
import com.ril.lib.TestsBase;
import com.ril.lib.Validator;
import com.ril.lib.utils;

/**
 * MyAccountTest Test the functionality related to my account.
 * 
 * @author Shankar.Kumar
 *
 */
public class MyAccountTest extends TestsBase {

	@DataProvider(name = "MyAccountData")
	public static Object[][] testData() throws Exception {
		ReadExcel re = new ReadExcel(utils.getConfig("DataSheet"),
		"MyProfile");
		//ReadExcel re = new ReadExcel(utils.getConfig("DataSheet"), "test1");
		return re.getTableToHashMapDoubleArray();
	}

	@Test(dataProvider = "MyAccountData")
	public void myAccountTest(HashMap<String, String> hm) throws Exception {
		try {
			if (hm.get("ExecuteFlag").trim().equalsIgnoreCase("N")) {
				throw new SkipException(
						"The test case has been skipped in test data skip flag");
			}
			driver.get(utils.getConfig("Url"));
			// maximize the window
			driver.manage().window().maximize();
			click(getLocator("Home", "signInLink"), 5);
			performSignIn(hm.get("UserName"), hm.get("Password"));
			click(getLocator("Home", "myAccountLink"), 10);
			/*
			 * After successful login perform different checks
			 */
			Validator.isTextPresent(driver, "My Account");

			switch (hm.get("FunctionalityCheck")) {
			case "ACC_UPDATE_CREDIT_CHK":
				// Make the store credit text
				Validator.isTextPresent(driver, "My Store Credit Balance");
				Validator.verifyTextForElement(driver,
						getLocator("MyAccount", "creditAvailable"),
						"My Store Credit Balance", 10);
				Validator.verifyTextForElement(driver,
						getLocator("MyAccount", "creditAvailable"),
						hm.get("I_AccountBal"), 10);
				click(getLocator("MyAccount", "editBtn"), 10);
				setValue(getLocator("MyAccount", "firstName"),
						"New First Name", 10);
				setValue(getLocator("MyAccount", "lastName"), "New Last Name",
						10);
				click(getLocator("MyAccount", "update"), 10);
				click(getLocator("MyAccount", "editBtn"), 20);
				verifyProfile_PersonalData("Mr.", "New First Name",
						"New Last Name", " ", " ");
				setValue(getLocator("MyAccount", "firstName"),
						hm.get("I_Fname"), 10);
				setValue(getLocator("MyAccount", "lastName"),
						hm.get("I_Lname"), 10);
				click(getLocator("MyAccount", "update"));
				// verifyProfile();
				break;

			case "MANDATORY_FIELD_CHECK":
				click(getLocator("MyAccount", "editBtn"));
				if (hm.get("I_Fname").equalsIgnoreCase("<BLANK>")) {
					setValue(getLocator("MyAccount", "firstName"), " ", 10);
				} else if (hm.get("I_Lname").equalsIgnoreCase("<BLANK>")) {
					setValue(getLocator("MyAccount", "lastName"), " ", 10);
				} else if (hm.get("I_MobNum").equalsIgnoreCase("<BLANK>")) {
					setValue(getLocator("MyAccount", "mobileNo"), " ", 10);
				} else if (hm.get("I_AddressName").equalsIgnoreCase("<BLANK>")) {
					setValue(getLocator("MyAccount", "addressName"), " ", 10);
				} else if (hm.get("I_FlatNo").equalsIgnoreCase("<BLANK>")) {
					setValue(getLocator("MyAccount", "flatNo"), " ", 10);
				} else if (hm.get("I_Society").equalsIgnoreCase("<BLANK>")) {
					setValue(getLocator("MyAccount", "building"), " ", 10);
				} else if (hm.get("I_Address1").equalsIgnoreCase("<BLANK>")) {
					setValue(getLocator("MyAccount", "address1"), " ", 10);
				} else if (hm.get("I_Pincode").equalsIgnoreCase("<BLANK>")) {
					setValue(getLocator("MyAccount", "pinCode"), " ", 10);
				}
				click(getLocator("MyAccount", "update"));
				Validator.verifyTextForElement(driver,
						getLocator("MyAccount", "tooltipError"),
						hm.get("O_ErrorMessage"), 5);

				break;

			case "PROFILE_UPDATE_NOT_SAVED":
				click(getLocator("MyAccount", "editBtn"), 10);
				fillPersonalDetail("Mr.", "NewFName", "NewLName", " ", " ");
				click(getLocator("MyAccount", "cancel"), 10);
				click(getLocator("MyAccount", "editBtn"), 20);
				verifyProfile_PersonalData(hm.get("I_Title"),
						hm.get("I_Fname"), hm.get("I_Lname"), " ", " ");
				break;

			case "PROFILE_UPDATE_BROWSER_CLOSE":
				click(getLocator("MyAccount", "editBtn"), 10);
				fillPersonalDetail("Mr.", "NewFName", "NewLName", " ", " ");
				driver.close();
				// Code needs to completed here still
				/*
				 * driver=new FirefoxDriver();
				 * driver.get(utils.getConfig("Url")); // maximize the window
				 * driver.manage().window().maximize(); click(getLocator("Home",
				 * "signInLink"), 5); performSignIn(hm.get("UserName"),
				 * hm.get("Password")); click(getLocator("Home",
				 * "myAccountLink"), 10); click(getLocator("MyAccount",
				 * "editBtn"), 10); click(getLocator("MyAccount", "editBtn"),
				 * 20); verifyProfile_PersonalData(hm.get("I_Title"),
				 * hm.get("I_Fname"), hm.get("I_Lname"), " ", " ");
				 */
				break;
			case "ADD_NEW_ADDRESS":
				click(getLocator("MyAccount", "editBtn"), 10);
				fillPersonalDetail(hm.get("I_Title"), hm.get("I_Fname"),
						hm.get("I_Lname"), hm.get("I_MobNum"), hm.get("I_DOB"));
				fillAddressDetail(hm.get("I_AddressName"), hm.get("I_FlatNo"),
						hm.get("I_Society"), hm.get("I_Address1"),
						hm.get("I_Address2"), hm.get("I_Pincode"),
						hm.get("I_Locality"));
				click(getLocator("MyAccount", "update"), 10);
				click(getLocator("MyAccount", "editBtn"), 20);
				verifyProfile_PersonalData(hm.get("I_Title"),
						hm.get("I_Fname"), hm.get("I_Lname"),
						hm.get("I_MobNum"), hm.get("I_DOB"));
				verifyProfile_AddressData(hm.get("I_AddressName"), hm.get("I_FlatNo"),
						hm.get("I_Society"), hm.get("I_Address1"),
						hm.get("I_Address2"), hm.get("I_Pincode"),
						hm.get("I_Locality"));
				break;
			default:
				break;
			}

		} catch (SkipException se) {
			logger.info("Test case skipped in skip flag of test data");

		}

	}

	public void performSignIn(String userName, String password)
			throws Exception {
		setValue(getLocator("SignIn", "LoginIDTxt"), userName, 20);
		setValue(getLocator("SignIn", "Password"), password, 20);
		click(getLocator("SignIn", "SignInBtn"), 10);
	}

	public void verifyProfile_PersonalData(String title, String FName,
			String LName, String mobNum, String dob) throws Exception {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		boolean condition = false;
		if (!title.trim().isEmpty()) {
			condition = driver.findElement(getLocator("MyAccount", "title"))
					.getAttribute("value").trim().equalsIgnoreCase(title);
			Assert.assertTrue(condition, "Update of title is not true");
		}
		if (!FName.trim().isEmpty()) {
			condition = driver
					.findElement(getLocator("MyAccount", "firstName"))
					.getAttribute("value").trim().equalsIgnoreCase(FName);
			Assert.assertTrue(condition, "Update of first name is not true");
		}
		if (!LName.trim().isEmpty()) {
			condition = driver.findElement(getLocator("MyAccount", "lastName"))
					.getAttribute("value").trim().equalsIgnoreCase(LName);
			Assert.assertTrue(condition, "Update of last name is not true");
		}

		if (!mobNum.trim().isEmpty()) {
			condition = driver.findElement(getLocator("MyAccount", "mobileNo"))
					.getAttribute("value").trim().equalsIgnoreCase(mobNum);
			Assert.assertTrue(condition, "Update of Mobile Number is not true");
		}

		if (!dob.trim().isEmpty()) {
			condition = driver.findElement(getLocator("MyAccount", "dob"))
					.getAttribute("value").trim().equalsIgnoreCase(dob);
			Assert.assertTrue(condition, "Update of DOB is not true");
		}
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
	}
	
	

	public void fillPersonalDetail(String title, String FName, String LName,
			String mobNum, String dob) throws Exception {
		if (!title.trim().isEmpty()) {
			selectValueinDropDown(getLocator("MyAccount", "title"), title, 10);
		}
		if (!FName.trim().isEmpty()) {
			setValue(getLocator("MyAccount", "firstName"), FName, 10);
		}
		if (!LName.trim().isEmpty()) {
			setValue(getLocator("MyAccount", "lastName"), LName, 10);
		}
		if (!mobNum.trim().isEmpty()) {
			setValue(getLocator("MyAccount", "mobileNo"), mobNum, 10);
		}
		if (!dob.trim().isEmpty()) {
			selectDOB(dob);
		}
	}

	public void selectDOB(String dob) throws Exception {
		String[] dobSplitPart = dob.split("-");
		if (!(dobSplitPart.length == 3)) {
			logger.error("There seems to be an error in dob string format");
			Assert.fail("There seems to be an error in dob string format");
		}

		click(getLocator("MyAccount", "dob"), 10);
		selectValueinDropDown(getLocator("MyAccount", "dobMonPicker"),
				dobSplitPart[1], 10);
		selectValueinDropDown(getLocator("MyAccount", "dobYearPicker"),
				dobSplitPart[2], 10);
		driver.findElement(By.linkText(dobSplitPart[0].trim())).click();
	}
	
	public void verifyProfile_AddressData(String addName, String flatNo,
			String buildingName, String addLine1, String addLine2,
			String pincode, String locality) throws Exception {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		boolean condition = false;
		if (!addName.trim().isEmpty()) {
			condition = driver.findElement(getLocator("MyAccount", "addressName"))
					.getAttribute("value").trim().equalsIgnoreCase(addName);
			Assert.assertTrue(condition, "Update of address name is not true");
		}
		if (!flatNo.trim().isEmpty()) {
			condition = driver
					.findElement(getLocator("MyAccount", "flatNo"))
					.getAttribute("value").trim().equalsIgnoreCase(flatNo);
			Assert.assertTrue(condition, "Update of flat number is not true");
		}
		if (!buildingName.trim().isEmpty()) {
			condition = driver.findElement(getLocator("MyAccount", "building"))
					.getAttribute("value").trim().equalsIgnoreCase(buildingName);
			Assert.assertTrue(condition, "Update of building name is not true");
		}

		if (!addLine1.trim().isEmpty()) {
			condition = driver.findElement(getLocator("MyAccount", "address1"))
					.getAttribute("value").trim().equalsIgnoreCase(addLine1);
			Assert.assertTrue(condition, "Update of address1 is not true");
		}

		if (!addLine2.trim().isEmpty()) {
			condition = driver.findElement(getLocator("MyAccount", "address2"))
					.getAttribute("value").trim().equalsIgnoreCase(addLine2);
			Assert.assertTrue(condition, "Update of address2 is not true");
		}
		
		if (!pincode.trim().isEmpty()) {
			condition = driver.findElement(getLocator("MyAccount", "pinCode"))
					.getAttribute("value").trim().equalsIgnoreCase(pincode);
			Assert.assertTrue(condition, "Update of pincode is not true");
		}
		
		if (!locality.trim().isEmpty()) {
			condition = driver.findElement(getLocator("MyAccount", "locality"))
					.getAttribute("value").trim().equalsIgnoreCase(locality);
			Assert.assertTrue(condition, "Update of locality is not true");
		}
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
	}

	public void fillAddressDetail(String addName, String flatNo,
			String buildingName, String addLine1, String addLine2,
			String pincode, String locality) throws Exception {
		if (!addName.trim().equals(" ")) {
			setValue(getLocator("MyAccount", "addressName"), addName, 10);
		}
		if (!flatNo.trim().equals(" ")) {
			setValue(getLocator("MyAccount", "flatNo"), flatNo, 10);
		}
		if (!buildingName.trim().equals(" ")) {
			setValue(getLocator("MyAccount", "building"), buildingName, 10);
		}
		if (!addLine1.trim().equals(" ")) {
			setValue(getLocator("MyAccount", "address1"), addLine1, 10);
		}
		if (!addLine2.trim().equals(" ")) {
			setValue(getLocator("MyAccount", "address2"), addLine2, 10);
		}
		if (!pincode.trim().equals(" ")) {
			setValue(getLocator("MyAccount", "pinCode"), pincode, 10);
		}
		if (!locality.trim().equals(" ")) {
			selectValueinDropDown(getLocator("MyAccount", "locality"),
					locality, 10);
		}
	}

}
