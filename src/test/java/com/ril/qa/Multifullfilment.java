package com.ril.qa;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.testng.SkipException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ril.lib.ReadExcel;
import com.ril.lib.TestsBase;
import com.ril.lib.utils;

public class Multifullfilment extends TestsBase {
	Components comp = null;
	public static String rilHost = "https://www.reliancefreshdirect.com/webapp/wcs/stores/servlet/en/rilesite";
			//"http://uat.reliancefreshdirect.com/webapp/wcs/stores/servlet/rilesite";
	// "https://www.reliancefreshdirect.com/webapp/wcs/stores/servlet/en/rilesite";

	// public static String rilUrl = rilHost + "";
	@BeforeMethod
	public void init() throws Exception {

		comp = new Components(driver);
		driver.get(rilHost);
	}

	@DataProvider(name = "riltestdata")
	public Object[][] testdata() throws Exception {

		ReadExcel re = new ReadExcel(utils.getConfig("Fullfillment_Data"),
				"Multifullfilment");
		return re.getTableToHashMapDoubleArray();

	}

	@Test(dataProvider = "riltestdata")
	public void testdriver(HashMap<String, String> hm) throws Exception {
		if (hm.get("TestCase RUN").toString().equalsIgnoreCase("no")) {

			throw new SkipException("Skipped Test case name  "
					+ hm.get("TestCase name").toString());
		} else {
			riladdtocart(hm);
		}
	}

	public void riladdtocart(HashMap<String, String> hm) throws Exception// (HashMap<String,
																			// String>
																			// hm)
																			// throws
																			// Exception
	{

		comp.signin(hm);
		if (!StringUtils.isBlank(hm.get("primaryPincode").toString())) {
			comp.updatePrimaryPincode(hm);
		}

		// comp.addtocart("apple", "1");
		// System.out.println("676");
		comp.deleteCartAll();
		List<String> productList = comp.addtocartlist(hm);
		// System.out.println("676");
		comp.navigateToCart();
		comp.checkout(hm, productList);
	}

	/*
	 * @Test public void riladdtomylist() throws Exception {
	 * 
	 * comp.signin();
	 * 
	 * 
	 * }
	 */

}
