package com.ril.qa;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ril.lib.ReadExcel;
import com.ril.lib.TestsBase;
import com.ril.lib.Validator;
import com.ril.lib.utils;

/**
 * Test for Registration functionality
 * 
 * @author Priyanka.Srivastava
 *
 */
public class RegistrationTest extends TestsBase {

	@DataProvider(name = "RegistrationTestdata")
	public static Object[][] testData() throws Exception {
		ReadExcel re = new ReadExcel(utils.getConfig("DataSheet"),
				"Registration");
		return re.getTableToHashMapDoubleArray();

	}

	@Test(dataProvider = "RegistrationTestdata")
	public void testRegistration(HashMap<String, String> hm) throws Exception {
		if (hm.get("Reg_ExecuteFlag").trim().equalsIgnoreCase("No")){
			throw new SkipException(
					"Skipping the test ---->> As per excel entry");
		}
		
		logger.info("Test execution for: test case number>> "
				+ hm.get("Reg_Id") + " **** " + hm.get("Reg_Description"));
		driver.get(utils.getConfig("Url"));
		// driver.get("http://uat.reliancefreshdirect.com/");
		driver.manage().window().maximize();
		// Check if the page is loaded
		//isHomePageLoaded();
		/*Assert.assertTrue(
				driver.getTitle().equalsIgnoreCase(
						utils.getConfig("HomePageTitle")),
				"The page title doesn't matched. Page" + "didn't load");*/
		// click on register link
		click(getLocator("Home", "registerLink"), 5);
		// check register page loaded

		performRegistration(hm);
		}

	

	public void performRegistration(HashMap<String, String> hm)
			throws Exception {

		driver.findElement(By.id("AddressEntryForm_FormInput_selectZipCode_1"))
				.sendKeys(hm.get("Reg_Pincode"));
		if (hm.get("Reg_IsPositive").equalsIgnoreCase("Ns")) {

			boolean isPass = false;
			String errorMsg1 = hm.get("Reg_ErrorMessage");
			try {
				driver.findElement(By.id("ErrorMessageText")).getText()
						.equalsIgnoreCase(errorMsg1);
				isPass = true;
			} catch (Exception e) {

				logger.error("Error message is not in message text.");

			}

			if (isPass) {
				logger.info("Error message found");
				return;
			} else
				logger.error("No error message found.");
			Assert.fail("No error message found.");
		}

		Thread.sleep(3 * 1000);
		WebElement element = driver.findElement(By
				.id("WC__AddressEntryForm_FormInput_selectCity"));
		element.click();
		String text = element.getAttribute("value");
		Assert.assertTrue(text.contains(hm.get("Reg_City")));

		WebElement element1 = driver.findElement(By.id("selectLocation"));
		element.click();
		String text1 = element1.getAttribute("value");
		System.out.println("Locality is--------------------:"+ text1);
		Assert.assertTrue(text1.contains(hm.get("Reg_Locality")));

		WebElement element2 = driver.findElement(By
				.id("WC__AddressEntryForm_FormInput_selectCity_1"));
		element.click();
		String text2 = element2.getAttribute("value");
		Assert.assertTrue(text2.contains(hm.get("Reg_State")));
        setValuesToFields(hm);

		if (hm.get("Reg_IsPositive").equalsIgnoreCase("No")) {

			boolean isPass1 = false;
			String errorMsg = hm.get("Reg_ErrorMessage");
			try {
				driver.findElement(
						By.xpath(".//*[@id='dijit__MasterTooltip_0']/div[1]"))
						.getText().equalsIgnoreCase(errorMsg);
				isPass1 = true;
			} catch (Exception e) {

				logger.error("Error message is not in tool tip.");
			}

			try {
				driver.findElement(By.id("UserRegistrationErrorMessage "))
						.getText().equalsIgnoreCase(errorMsg);
				isPass1 = true;
			} catch (Exception e) {

				logger.error("Error message is not in page errors.");
			}

			if (isPass1) {
				logger.info("Error message found");
			} else {
				logger.error("No error message found.");
				Assert.fail("No error message found.");

			}

		} 

		else {

			Boolean regSuccessful = Validator.isTextPresent(driver, "You have successfully registered");
			if(regSuccessful.equals(true))
				logger.info("Registration successful");
			else
				{
				logger.info("Registration failed");
				Assert.fail("Registration failed");
				}
			
		}
		
	
	}

	public void isHomePageLoaded() {
		// Check if the page is loaded
		Assert.assertTrue(
				driver.getTitle().equalsIgnoreCase(
						utils.getConfig("HomePageTitle")),
				"The page title doesn't matched. Page" + "didn't load");
	}

	public void isRegistrationPageLoaded() {
		// Check if the page is loaded
		Assert.assertTrue(driver.getTitle().equalsIgnoreCase(("Register")),
				"The page title doesn't matched. Page" + "didn't load");
	}
	
	public void setValuesToFields(HashMap<String, String> hm) throws Exception{
		
	

		setValue(getLocator("Registration", "LogIn"), hm.get("Reg_Email_Id"));
		setValue(getLocator("Registration", "Pwd"), hm.get("Reg_Pwd"));
		setValue(getLocator("Registration", "ConfPwd"), hm.get("Reg_Conf_Pwd"));
		setValue(getLocator("Registration", "ConfPwd"), hm.get("Reg_Conf_Pwd"));
		setValue(getLocator("Registration", "ConfPwd"), hm.get("Reg_Conf_Pwd"));
		setValue(getLocator("Registration", "RoneCard"), hm.get("Reg_RoneCard"));
		setTitle(hm);
		setValue(getLocator("Registration", "FirstName"), hm.get("Reg_Fname"));
		setValue(getLocator("Registration", "LastName"), hm.get("Reg_Lname"));
		setValue(getLocator("Registration", "MobileNo"), hm.get("Reg_Mobile"));
		System.out.println("Mobile no is---------------"+ hm.get("Reg_Mobile"));
		setDate(hm);
		setValue(getLocator("Registration", "AddressName"),
				hm.get("Reg_Address_Name"));
		setValue(getLocator("Registration", "FlatNo"), hm.get("Reg_FlatNo"));
		setValue(getLocator("Registration", "Building"), hm.get("Reg_Building"));
		setValue(getLocator("Registration", "AddressLine1"),
				hm.get("Reg_AddrLine1"));
		setValue(getLocator("Registration", "AddressLine2"),
				hm.get("Reg_AddrLine2"));
		click(getLocator("Registration", "IAgree"), 5);
		click(getLocator("Registration", "RegBtn"), 5);
		
		
		
	}

	
	public void setTitle(HashMap<String, String> hm) throws Exception {

		String title = hm.get("Reg_Title");
		Select dropdown = new Select(driver.findElement(getLocator(
				"Registration", "Title")));
		dropdown.selectByValue(title);

	}

	public void setDate(HashMap<String, String> hm) throws Exception {

		String year = hm.get("Reg_Year");
		String month = hm.get("Reg_Month");
		String date = hm.get("Reg_Date");
		click(getLocator("Registration", "DOfBirth"));

		Select yearSelect = new Select(driver.findElements(
				By.xpath(".//*[@id ='ui-datepicker-div']/div/div/select")).get(
				1));
		yearSelect.selectByValue(year);

		Select monthSelect = new Select(driver.findElements(
				By.xpath(".//*[@id ='ui-datepicker-div']/div/div/select")).get(
				0));
		monthSelect.selectByValue(month);

		List<WebElement> weeks = driver.findElements(By
				.xpath(".//*[@id='ui-datepicker-div']/table/tbody/tr"));

		int weekNo = 0;
		for (WebElement week : weeks) {
			List<WebElement> days = week.findElements(By
					.xpath(".//*[@id='ui-datepicker-div']/table/tbody/tr["
							+ weekNo + "]"));
			try {
				week.findElement(By.linkText(date)).click();
				logger.info("Date present in week " + weekNo);
			} catch (Exception e) {
				logger.error("Date not present in week " + weekNo);
			}

			weekNo++;
		}

	}

}
