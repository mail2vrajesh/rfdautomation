package com.ril.qa;

import java.util.HashMap;

import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ril.lib.ReadExcel;
import com.ril.lib.TestsBase;
import com.ril.lib.Validator;
import com.ril.lib.utils;

/**
 * 
 * @author Shankar.Kumar
 *
 */

public class MyListTest extends TestsBase {
	@DataProvider(name = "MyListData")
	public static Object[][] testData() throws Exception {
		ReadExcel re = new ReadExcel(utils.getConfig("DataSheet"), "MyList");
		return re.getTableToHashMapDoubleArray();
	}

	@Test(dataProvider = "MyListData")
	public void myListTest(HashMap<String, String> hm) throws Exception {
		try {
			if (hm.get("ExecuteFlag").trim().equalsIgnoreCase("N")) {
				throw new SkipException(
						"The test case has been skipped in test data skip flag");
			}
			driver.get(utils.getConfig("Url"));
			// maximize the window
			driver.manage().window().maximize();
			click(getLocator("Home", "signInLink"), 5);
			performSignIn(hm.get("UserName"), hm.get("Password"));
			logger.info("Testing the my list link from the header link");

			switch (hm.get("FunctionalityCheck")) {
			case "ADD_NEW_LIST":
				click(getLocator("Home", "myListLink"), 10);
				Validator.verifyTextForElement(driver,
						getLocator("MyList", "headerName"), "My List", 10);
				logger.info("Testing the my list link from the left side pane now");
				click(getLocator("MyAccount", "myListLink"), 10);
				Validator.verifyTextForElement(driver,
						getLocator("MyList", "headerName"), "My List", 10);
				click(getLocator("MyList", "createNewListBtn"), 10);
				setValue(getLocator("createNewListPopup", "newListTxt"),
						hm.get("I_NewListName"), 10);
				click(getLocator("createNewListPopup", "saveBtn"), 10);
				Validator.verifyTextForElement(driver,
						getLocator("CreateListPopUp", "successText"),
						"List created successfully.", 10);
				click(getLocator("CreateListPopUp", "continueShoppingBtn"), 10);
				Validator.verifyTextForElement(driver,
						getLocator("MyList", "headerName"), "My List", 10);
				Validator.isElementPresent(driver, getLocator("MyList", "changeListNameBtn"), 10);
				Validator.isElementPresent(driver, getLocator("MyList", "deleteListBtn"), 10);
				break;

			default:
				break;
			}

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public void performSignIn(String userName, String password)
			throws Exception {
		setValue(getLocator("SignIn", "LoginIDTxt"), userName, 20);
		setValue(getLocator("SignIn", "Password"), password, 20);
		click(getLocator("SignIn", "SignInBtn"), 10);
	}

}
