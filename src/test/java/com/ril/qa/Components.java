package com.ril.qa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.SendKeysAction;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.ril.lib.TestsBase;
import com.ril.lib.WaitEvent;
import com.ril.lib.utils;
import com.thoughtworks.selenium.webdriven.commands.Click;

public class Components extends TestsBase {
	// UIMapParser ui;

	public Components(WebDriver d) throws Exception {
		driver = d;
		we = new WaitEvent(driver);
		utils.setConfiguration("src/test/resources/config.properties");
		// ui = new UIMapParser(utils.getConfig("ObjectRepository"));
		setupUIMapParser();
	}

	public void signin(HashMap hm) throws Exception {
		// .get("http://uat.reliancefreshdirect.com/webapp/wcs/stores/servlet/en/rilesite");

		driver.manage().window().maximize();
		getLocator("signin", "username");

		click(By.id("SignInLink"), 10);
		setValue(By.name("logonId"), hm.get("Username").toString());
		setValue(By.name("logonPassword"), hm.get("Password").toString());
		click(By.id("WC_AccountDisplay_links_2"));

		/*
		 * click( getLocator("home", "signin"));
		 * 
		 * setValue(getLocator("signin", "username"),"santhosh.baby@ril.com");
		 * setValue(getLocator("signin", "password"), "test@123");
		 * click(getLocator("signin", "signin"));
		 */
	}

	public String addToCart(String productname, String qty, String inoutstock, String PDPpage)
			throws Exception {

		setValue(By.id("SimpleSearchForm_SearchTerm"), productname);
		click(By.id("search_submit"));
		List<WebElement> producttext = driver
				.findElements(By
						.xpath(".//*[@id='searchBasedNavigation_widget']//span[contains(@id,'highlightedName')]"));

		List<WebElement> buttons = driver
				.findElements(By
						.xpath(".//*[@id='searchBasedNavigation_widget']/div[1]/div/div[2]//div[1]//div[@class='button_text']"));
		List<WebElement> input = driver
				.findElements(By
						.xpath(".//*[@id='searchBasedNavigation_widget']/div[1]/div/div[2]/fieldset/div/div//input[contains(@id,'quantity')]"));

		if(PDPpage.equalsIgnoreCase("Yes"))
		{
			click(By.xpath("(.//*[@id='searchBasedNavigation_widget']//a[contains(@id,'WC_CatalogEntryDBThumbnailDisplay')])[1]"), 10);

	    	String pdpstockmsg=	getText(By.xpath(".//span[ contains(@id,'InventoryStatus_OnlineStatus')]"), 10);
			
			if (pdpstockmsg.equalsIgnoreCase("In-Stock")
					&& inoutstock.equalsIgnoreCase("instock")) {

			setValue(By.xpath(".//input[contains(@id,'quantity')]"), qty);
			click(By.id("add2CartBtn"));
			
				Assert.assertEquals(
						getText(By.id("itemName")).trim(),
						getText(
								By.xpath(".//*[@id='widget_minishopcart_popup_1']//div[@class='product_name']"),
								10).toString().trim());

				driver.findElement(
						By.xpath(".//*[@id='widget_minishopcart_popup_1']//span[@class='close']"))
						.click();
				System.out.println(getText(By.id("itemName")).trim());
				return getText(By.id("itemName")).trim();
			}

			
			if (qty.equalsIgnoreCase("1") && pdpstockmsg.equalsIgnoreCase("Out of Stock")
					&& inoutstock.equalsIgnoreCase("outofstock")) {

				String geterror= getText(By.id("errorDescription"), 10);
				Assert.assertEquals("We're sorry, Item is not available in selected store area", geterror.trim());
				return "NO product";
			}
			
			if ( !qty.equalsIgnoreCase("1") && inoutstock.equalsIgnoreCase("outofstock")) {


				setValue(By.xpath(".//input[contains(@id,'quantity')]"), qty);
				click(By.id("add2CartBtn"));
				
				waitDialog("outofstock");

				return "NO product";
			}
		

			Assert.fail("Excel sheet values not filled up correctly");
			
		}
		
		
		for (int i = 0; i <=0; i++) {
			input.get(i).clear();
			input.get(i).sendKeys(qty);
			buttons.get(i).click();

			System.out.println("Click  " + i);

			if (waitDialog(inoutstock)
					&& inoutstock.equalsIgnoreCase("instock")) {

				Assert.assertEquals(
						producttext.get(i).getText().trim(),
						getText(
								By.xpath(".//*[@id='widget_minishopcart_popup_1']//div[@class='product_name']"),
								10).toString().trim());

				driver.findElement(
						By.xpath(".//*[@id='widget_minishopcart_popup_1']//span[@class='close']"))
						.click();
				System.out.println(producttext.get(i).getText());
				return producttext.get(i).getText();
			}

			if (!waitDialog(inoutstock)
					&& inoutstock.equalsIgnoreCase("outofstock")) {

				return "NO product";
			}

		}
		return "Wrong input data  oberseved  . Valid input  instock  or outofstock ";

	}

	public Boolean waitDialog(String stock) throws Exception {

		int durationSecs = 7;

		if (we == null)
			we = new WaitEvent(driver);

		if (stock.equalsIgnoreCase("outofstock")) {
			try {

				WebElement ebox = we.waitForElement(
						By.id("msgpopup_content_wrapper"), durationSecs);

				if (ebox.isDisplayed()) {
					System.out.println("found OOS pop up");
					click(By.xpath(".//*[@id='msgpopup_content_wrapper']/div[3]/a/div[2]"));
					return false;
				}
			} catch (Exception e) {
				Assert.fail("Out of stock product  notification failed to observe");
			}
		}

		if (stock.equalsIgnoreCase("instock")) {

			try {

				WebElement ebox = we.waitForElement(
						By.id("msgpopup_content_wrapper"), durationSecs);

				if (ebox.isDisplayed()) {
					System.out.println("found OOS pop up");
					click(By.xpath(".//*[@id='msgpopup_content_wrapper']/div[3]/a/div[2]"));
					Assert.fail("Out of stock product  notification Shown for a instock product");
				}

			} catch (Exception e) {
				return true;
			}

		}

		return true;

	}

	

	public void deleteCartAll() throws Exception {
		navigateToCart();
		try{
			click(getLocator("cartpage", "deleteall"));
			click(By.xpath(".//*[@id='confirmDeleteMsgpopup_content_wrapper']/div[3]/a[2]")); // Yes pop up - a[2], No pop up - a[1]
		}
		catch(Exception e)
		{
			logger.info("Delete All button not available");
		}
		
	}

	public void deleteCartItem(int itemNum) throws Exception {
		navigateToCart();
		try{
		WebElement itemToDelete = driver.findElements(By.id("cartlistitem"))
				.get(itemNum);
		itemToDelete.click();
		
		}
		catch(Exception e)
		{
			logger.info("Item to delete not available");
		}

	}

	public void navigateToCart() throws Exception {
		WebElement webElement = getWebElement(getLocator("minicart", "body"));
		Actions action = new Actions(driver);
		action.moveToElement(webElement).perform();
		click(getLocator("minicart", "gotocart"));
	}

	public void checkout(HashMap<String, String> params,
			List<String> productList) throws Exception {
		logger.info("Entered checkout");

		String slotDate = (String) params.get("slotDate");
		int slotNum = Integer.parseInt(params.get("slotNum"));
		String address = params.get("address");
		String payMethod = params.get("payMethod");
		String payMedium = params.get("payMedium");
		String addrChange = params.get("ChangeAddress");
		List<String> prodStocks = new ArrayList<String>();
		prodStocks.add(params.get("Product1OOSAddr"));
		prodStocks.add(params.get("Product2OOSAddr"));
		prodStocks.add(params.get("Product3OOSAddr"));
		boolean isFlowRedirected = false;

		List<Integer> outOfStockProds = new ArrayList<Integer>();
		for (String prodStock : prodStocks) {
			if (prodStock.equalsIgnoreCase("OutOfStock")) {
				outOfStockProds.add(prodStocks.indexOf(prodStock));
				isFlowRedirected = true;
			}
		}

		
		click(getLocator("cartpage", "checkoutbtn"));
		selectValueinDropDown(getLocator("checkout", "selectaddress"), address);
		
		

		
			
		if(addrChange.equalsIgnoreCase("Yes"))
		{
			int durationSecs = 7;

			if (we == null)
				we = new WaitEvent(driver);

		

					WebElement epopup = we.waitForElement(
							By.xpath(".//*[@id='confirmMsgpopup_content_wrapper']/div[3]/a[2]"), durationSecs);

			epopup.click();
			
		}
		
		for (Integer oos : outOfStockProds) {
			int durationSecs = 7;
			WaitEvent tempWE = new WaitEvent(driver);
			WebElement productError = tempWE.waitForElement(By.xpath(".//*[@id='shoppingCart_rowHeader_product" + (oos+1)
					+ "']/div[3]"),durationSecs);
			String prodNameAndOOSError = productError.getText();
			Assert.assertEquals(prodNameAndOOSError.trim(),
					"This item is not available in stock");
			WebElement itemToDelete = driver.findElements(By.id("cartlistitem"))
					.get(oos);
			itemToDelete.click();
			productList.remove((int)oos);
			
		}
		
	List<Integer> prodsToRemove = new ArrayList<Integer>();	
	for(String prod: productList)
	{
		if(prod.contains("NO product"))
		{
			prodsToRemove.add(productList.indexOf(prod));
		}
	}
	
	for(int indxProdToRemove: prodsToRemove)
	{
		productList.remove(indxProdToRemove);
	}
	
	System.out.println("Product List: "+productList);
	
		if(isFlowRedirected)
		{
			int durationSecs = 7;
			WaitEvent tempWE = new WaitEvent(driver);
			WebElement newCheckoutBtn = tempWE.waitForElement(getLocator("cartpage", "checkoutbtn"),durationSecs);
			newCheckoutBtn.click();
		}
		/*Thread.sleep(5*1000);
		takeScreenshot("D:\\Tests\\Screenshots\\test_screen.jpg");*/
		
		String addrVal = getText(By.xpath(".//*[@id='shippingAddressDisplayArea']/dl"));
		addrVal = addrVal.replaceAll("\\s", "");
		addrVal = addrVal.replaceAll(":", "");
		click(By.xpath(".//*[@id='slot_" + slotNum + "_" + slotDate + "']/a"));
		click(getLocator("checkout", "nextbtn"));

		if (payMethod.equalsIgnoreCase("CODOthers")) {
			click(getLocator("payment", "codandothers"));
		} else if (payMethod.equalsIgnoreCase("NetBanking")) {
			click(getLocator("payment", "netbanking"));
			Thread.sleep(3 * 1000);
			selectValueinDropDown(getLocator("payment", "selectbank"),
					payMedium);
		} else if (payMethod.equalsIgnoreCase("CreditCard")) {
			click(getLocator("payment", "creditcard"));
			if (payMedium.equalsIgnoreCase("Visa")) {
				click(getLocator("payment", "visacreditcard"));
			} else if (payMedium.equalsIgnoreCase("Master")) {
				click(getLocator("payment", "mastercreditcard"));
			} else if (payMedium.equalsIgnoreCase("AmEx")) {
				click(getLocator("payment", "amexcreditcard"));
			} else if (payMedium.equalsIgnoreCase("AmexEzeClick")) {
				click(getLocator("payment", "amexezeclickcreditcard"));
			} else {
				logger.error("Invalid Credit Card Type");
			}
		} else if (payMethod.equalsIgnoreCase("DebitCard")) {
			click(getLocator("payment", "debitcard"));

			if (payMedium.equalsIgnoreCase("Visa")) {
				click(getLocator("payment", "visadebitcard"));
			} else if (payMedium.equalsIgnoreCase("Master")) {
				click(getLocator("payment", "masterdebitcard"));
			} else if (payMedium.equalsIgnoreCase("Maestro")) {
				click(getLocator("payment", "maestrodebitcard"));
			} else if (payMedium.equalsIgnoreCase("Rupay")) {
				click(getLocator("payment", "rupaydebitcard"));
			} else {
				logger.error("Invalid Debit Card Type");
			}
		} else {
			logger.error("Invalid Payment Type");
		}

		click(getLocator("payment", "termsandconditions"));
		click(getLocator("payment", "nextbtn"));
		Thread.sleep(5 * 1000);
		String summaryAddr = driver.findElement(
				getLocator("ordersummary", "address")).getText();
		summaryAddr = summaryAddr.replaceAll("\\s", "");
		summaryAddr = summaryAddr.replaceAll(":", "");

		int prodIndex = 0;
		ArrayList<Boolean> isProdsMatch = new ArrayList<Boolean>();
		ArrayList<Boolean> isQtyMatch = new ArrayList<Boolean>();
		ArrayList<String> expProdList = new ArrayList<String>();
		for (String prod : productList) {
			prodIndex++;
			String prodName = prod.split(",")[0];
			String prodQty = prod.split(",")[1];
			String prodHeadId = "SingleShipment_rowHeader_product" + prodIndex;
			String prodQtyId = "WC_OrderItemDetailsSummaryf_td_2_" + prodIndex;
			
			String summaryProdName = getText(By.id(prodHeadId));
			/*String summaryProdName = driver.findElement(By.id(prodHeadId))
					.getText();*/
			String summaryProdQty = getText(By.id(prodQtyId));
		
			expProdList.add(summaryProdName + "," + summaryProdQty);
			if (summaryProdName.contains(prodName)) {
				isProdsMatch.add(true);
			} else {
				isProdsMatch.add(false);
			}

			if (prodQty.equalsIgnoreCase(summaryProdQty)) {
				isQtyMatch.add(true);
			} else {
				isQtyMatch.add(false);
			}
		}

		boolean isAddressMatch = true;
		try {
			Assert.assertEquals(addrVal, summaryAddr);
		} catch (AssertionError ae) {
			isAddressMatch = false;
		}

		click(getLocator("ordersummary", "paynowbtn"));
		Thread.sleep(3 * 1000);
		
		if (isAddressMatch) {
			logger.info("Selected address matches with summary page");
		} else {
			logger.error("Selected address doesn't match with summary page.");
			Assert.fail("Selected address doesn't match with summary page.");
		}

		boolean isProdFail = false;
		prodIndex = 0;
		for (Boolean isProdMatch : isProdsMatch) {
			prodIndex++;
			if (isProdMatch) {
				logger.info("Product" + prodIndex + ":"
						+ productList.get(prodIndex - 1)
						+ " is same on summary page");
			} else {
				logger.error("Product" + prodIndex + ":"
						+ productList.get(prodIndex - 1)
						+ " is not same on summary page");
				isProdFail = true;
			}
		}

		prodIndex = 0;
		for (Boolean isQtMatch : isQtyMatch) {
			prodIndex++;
			if (isQtMatch) {
				logger.info("Product" + prodIndex + ":"
						+ productList.get(prodIndex - 1)
						+ "'s quantity is same on summary page");
			} else {
				logger.info("Product" + prodIndex + ":"
						+ productList.get(prodIndex - 1)
						+ "'s quantity is not same on summary page");
				isProdFail = true;
			}
		}

		

		if (isAddressMatch) {
			logger.info("Selected address matches with summary page");
		} else {
			logger.error("Selected address doesn't match with summary page.");
			Assert.fail("Selected address doesn't match with summary page.");
		}

		if (isProdFail) {
			Assert.fail("There are mismatch in product details on summary page.");
		}

	}

	public String getOrderTableCellText(WebElement prodTab, int col, int row) {
		String cellVal = null;

		if (col == 1) {
			cellVal = driver
					.findElement(
							By.xpath(".//*[@id='ShopCartPagingDisplay']/table/tbody/tr["
									+ (row + 1) + "]/th[" + (col) + "]"))
					.getText();
		} else {
			cellVal = driver
					.findElement(
							By.xpath(".//*[@id='ShopCartPagingDisplay']/table/tbody/tr["
									+ (row + 1) + "]/td[" + (col) + "]"))
					.getText();
		}

		return cellVal;
		// Just to see the update
	}

	public ArrayList addtocartlist(HashMap<String, String> hm) throws Exception {
		ArrayList al = new ArrayList<String>();
		if (!StringUtils.isBlank(hm.get("Product1").toString())
				&& !StringUtils.isBlank(hm.get("qty1")))
			al.add(addToCart(hm.get("Product1"), hm.get("qty1"),
					hm.get("Product1Stock").toString(),hm.get("PDP1Page").toString()).toString()
					+ "," + hm.get("qty1").toString());

		if (!StringUtils.isBlank(hm.get("Product2").toString())
				&& !StringUtils.isBlank(hm.get("qty2")))
			al.add(addToCart(hm.get("Product2"), hm.get("qty2"),
					hm.get("Product2Stock").toString(),hm.get("PDP2Page"))
					+ "," + hm.get("qty2"));

		if (!StringUtils.isBlank(hm.get("Product3").toString())
				&& !StringUtils.isBlank(hm.get("qty3")))
			al.add(addToCart(hm.get("Product3"), hm.get("qty3"),
					hm.get("Product3Stock").toString(),hm.get("PDP3Page"))
					+ "," + hm.get("qty3"));

		return al;

	}


	public void createNewList(String name) throws Exception{
	
	click(getLocator("home","MyList"),10);
	click(getLocator("MyList","CreateNewList"),10);
	setValue(getLocator("MyList","CreateNewListTextBox"), name);
	click(getLocator("MyList","SaveButton"),10);
	Thread.sleep(4000);
	try{
		Assert.assertEquals(driver.findElement(By.id("successMessageAreaText")).getText(), "List created successfully.");
		logger.info("List Created successfully");
	} catch(AssertionError Error){
		logger.info("unable to create a list");
	}
	finally{
		click(By.partialLinkText("Continue"));
	}
	
}


public void DeleteList(String name) throws Exception{
	click(getLocator("home","MyList"),10);
	Select dropdown=new Select(driver.findElement(By.id("multipleWishlistController_select")));
	dropdown.selectByVisibleText(name);
	click(getLocator("MyList","DeleteList"),10);
	click(getLocator("MyList","DeleteListYesButton"),10);
	Thread.sleep(5000);
	try{
		Assert.assertEquals(driver.findElement(By.id("successMessageAreaText")).getText(), "List deleted successfully.");
		logger.info("Deleted successfully");
	} catch(AssertionError Error){
		logger.info("unable to delete a list");
	}
	finally{
		click(By.partialLinkText("Continue"));
	}
}

public void AddProdToMyList(String catagory) throws Exception{
	setValue(getLocator("home","searchbox"),catagory,10);
	click(getLocator("home","searchBtn"),10);		
	driver.manage().window().maximize();
	List<WebElement> producttext = driver
			.findElements(By
					.xpath(".//*[@id='searchBasedNavigation_widget']//span[contains(@id,'highlightedName')]"));
	
	for(int i=1;i<producttext.size();i++){
		
		System.out.println(producttext.get(i));
	}
}

public void updatePrimaryPincode(HashMap<String, String> hm) throws Exception {
	
	click(getLocator("Home", "myAccountLink"), 10);
	click(getLocator("MyAccount", "editBtn"), 10);
	
	
	setValue(getLocator("AddressBook", "pincode"),
			hm.get("primaryPincode"), 10);
	click(getLocator("MyAccount", "update"), 20);
	
	
}


}
