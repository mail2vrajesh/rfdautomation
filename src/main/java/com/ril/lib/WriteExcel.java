package com.ril.lib;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import jxl.read.biff.BiffException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class WriteExcel {
	public static final Logger logger = LoggerFactory.getLogger(WriteExcel.class);
	
	private Workbook wb=null;

	Sheet sheet=null;
    private String xlFile=null;
    
	public WriteExcel(String xlsfile) throws IndexOutOfBoundsException,  IOException, InvalidFormatException {
		// TODO Auto-generated constructor stub
		this(xlsfile,1);
	}

	public WriteExcel(String xlsfile,String sheetName) throws IndexOutOfBoundsException,  IOException, InvalidFormatException {
		// TODO Auto-generated constructor stub
		xlFile=xlsfile;
		InputStream inp = new FileInputStream(xlsfile);
		wb = WorkbookFactory.create(inp);
	    sheet= wb.getSheet(sheetName);
		
	}
	

	/**
	 * Sheet number starts with 1
	 * @param xlsfile
	 * @param sheetNumber
	 * @throws IndexOutOfBoundsException
	 * @throws BiffException
	 * @throws IOException
	 * @throws InvalidFormatException 
	 */
	public WriteExcel(String xlsfile,int sheetNumber) throws IndexOutOfBoundsException,  IOException, InvalidFormatException {
		// TODO Auto-generated constructor stub
		xlFile=xlsfile;
		InputStream inp = new FileInputStream(xlsfile);
		wb = WorkbookFactory.create(inp);
	    sheet= wb.getSheetAt(sheetNumber-1);

	}
	
	
////////********************************* Table Write Methods*****************************************************
		/**
		 * @author Rajesh.Vemula
		 * row and columns starts with 1'
		 * @param row
		 * @param column
		 * @throws Exception
		 */
		public void WriteToTable(int row,int column,String value) throws Exception
		{
			try
			{		
					 Row r = sheet.getRow(row-1);
					 if (r==null)
						 r=sheet.createRow(row-1);
					 Cell cell = r.getCell(column);
				    if (cell == null)
				        cell = r.createCell(column);
				    cell.setCellType(Cell.CELL_TYPE_STRING);
				    cell.setCellValue(value);

				    // Write the output to a file
				    FileOutputStream fileOut = new FileOutputStream(xlFile);
				    wb.write(fileOut);
				    fileOut.close();
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
				logger.info("Failed to write to Excel " + e.getMessage());
				throw new Exception("Failed to write to Excel ");
			}
			
			
		}
		
		/**
		 * @author Rajesh.Vemula
		 * row starts with 1'
		 * @param row
		 * @param column
		 * @throws Exception
		 */
		public void WriteToTable(int row,String ColumnName,String value) throws Exception
		{
				WriteToTable(row, findColumnNumber(ColumnName), value);
			
		}
		
		
		public int findColumnNumber(String val)
		{
			Row row=sheet.getRow(0);
			for(Cell cell : row) 
			{
				if(cell.getStringCellValue().equalsIgnoreCase(val)) 
					return cell.getColumnIndex();
			 
			}
			return -1;

		}
}
