/**
 * 
 */
package com.ril.lib;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;




/**
 * @author Rajesh Vemula
 *
 */
public class SelBase {
	
	public static final Logger logger = LoggerFactory.getLogger(SelBase.class);
	protected WebDriver driver;
	protected WaitEvent we=null;
	public SelBase() {
		logger.debug("Constructor for SelBase was invoked");
		
		//we=new WaitEvent(driver);
	}
	/**
	 * 
	 *  Waits for the webelement until 'durationSecs' and Types value 'Val' in to the webelement whose locator value is 'locator'.
	 * @param locator
	 * @param val
	 * @param durationSecs
	 * @throws Exception
	 */
	public void setValue(By locator, String val,int durationSecs) throws Exception
	{
		logger.debug("Setting value "+ val + " for the UI element " + locator);
		if(durationSecs >0)
		{
			if(we==null)
				we= new WaitEvent(driver);
			
			we.waitForElement(locator, durationSecs);
		}
		driver.findElement(locator).clear();
		driver.findElement(locator).sendKeys(val);
		logger.debug("Succesfully Set the value "+ val + " for the UI element " + locator);
	}

	public void setValue(By locator, String val) throws Exception
	{
		setValue(locator, val, 0);
	}

	public void click(By locator,int durationSecs) throws Exception
	{
		logger.debug("Clicking the Web element " + locator);
		if(durationSecs >0)
		{
			if(we==null)
				we= new WaitEvent(driver);
			
			we.waitForElement(locator, durationSecs);
		}
		
		driver.findElement(locator).click();
		logger.debug("Succesfully Clicked the Web element " + locator);
	}
	
	public void click(By locator) throws Exception
	{
		click(locator, 0);
	}
	
	public void selectValueinDropDown(By locator, String text,int durationSecs) throws Exception
	{
		logger.debug("Selecting value "+ text + "in the dropdown " + locator);
		if(durationSecs >0)
		{
			if(we==null)
				we= new WaitEvent(driver);
			
			we.waitForElement(locator, durationSecs);
		}
		
		new Select(driver.findElement(locator)).selectByVisibleText(text);
		logger.debug("Succesfully selected "+ text + "in the dropdown " + locator);
	}
	
	public void selectValueinDropDown(By locator, String text) throws Exception
	{
		selectValueinDropDown(locator, text, 0);
	}

	
	public void selectValueUsingIndex(By locator, int index,int durationSecs) throws Exception
	{
		logger.debug("Selecting value at index "+ index + "in the dropdown " + locator);
		if(durationSecs >0)
		{
			if(we==null)
				we= new WaitEvent(driver);
			
			we.waitForElement(locator, durationSecs);
		}
		
		new Select(driver.findElement(locator)).selectByIndex(index);
		logger.debug("Succesfully selected value at index "+ index + "in the dropdown " + locator);
	}
	
	public void selectValueUsingIndex(By locator, int index) throws Exception
	{
		selectValueUsingIndex(locator, index, 0);
	}
	
	
	public void selectDropDownUsingHtmlValueAttribute(By locator, String text,int durationSecs) throws Exception
	{
		logger.debug("Selecting value "+ text + "in the dropdown " + locator);
		if(durationSecs >0)
		{
			if(we==null)
				we= new WaitEvent(driver);
			
			we.waitForElement(locator, durationSecs);
		}
		
		new Select(driver.findElement(locator)).selectByValue(text);
		logger.debug("Succesfully selected "+ text + "in the dropdown " + locator);
	}

	public void selectDropDownUsingHtmlValueAttribute(By locator, String text) throws Exception
	{
		selectDropDownUsingHtmlValueAttribute(locator, text,0);
	}

	/**
	 *  Returns gettext value of the locator
	 * @param locator
	 * @param durationSecs
	 * @throws Exception
	 */
	public String getText(By locator,int durationSecs) throws Exception
	{
		logger.debug("Trying to retrieve text value for " + locator);
		if(durationSecs >0)
		{
			if(we==null)
				we= new WaitEvent(driver);
			we.waitForElement(locator, durationSecs);
		}
		return driver.findElement(locator).getText();
	}
	
	/**
	 *  Returns gettext value of the locator
	 * @param locator
	 * @param durationSecs
	 * @throws Exception
	 */
	public String getText(By locator) throws Exception
	{
		return getText(locator,0);
	}
	
	
	// ------------------------------------------------------------------------
	// Private methods common to local/remote browsers
	// ------------------------------------------------------------------------
	protected void takeScreenshot(String fileName) {
	
	try {
			logger.debug("Screenshot file name: " + fileName);
			
			if (driver.getClass().toString().contains("Remote") )
			{
				// Save a screenshot
				WebDriver augmentedDriver = new Augmenter().augment(driver);
				byte screenshot[] = ((TakesScreenshot)augmentedDriver).getScreenshotAs(OutputType.BYTES);
				//driver.
				this.writeScreenshotToFile(screenshot,fileName);
				//augmentedDriver.close();
			} 
			else if (driver.getClass().toString().contains("firefox") || driver.getClass().toString().contains("ie")) 
			{
				byte screenshot[] = ((TakesScreenshot)this.driver).getScreenshotAs(OutputType.BYTES);
				this.writeScreenshotToFile(screenshot,fileName);
			} else {
				logger.warn("Screenshot NOT saved.");
				return;
			}
		}	
		catch(Exception e) {
			// Ignore errors while taking the screenshot
			e.printStackTrace();
			logger.info("Failed to capture screenshot! " + e.getMessage());
		}
	}
	
	private void writeScreenshotToFile(byte screenshot[], String fileName) throws IOException {
		File outFile = new File(fileName);
		FileOutputStream fout = new FileOutputStream(outFile);
		fout.write(screenshot);
		fout.flush();
		fout.close();
	}

	/**
	 * @author Rajesh.Vemula
	 * returns option value located at index (starts with 1)in the drop down
	 */
	public String getValInDropDown(By locator, int index)
	{
		 Select selectList = new Select(driver.findElement(locator));
		 List<WebElement> options=selectList.getOptions();
		 return options.get(index-1).getText();
		
	}
	
	/**
	 * 
	 */
	public boolean isValExistInDropDown(By locator, String val)
	{
		 Select selectList = new Select(driver.findElement(locator));
		 List<WebElement> options=selectList.getOptions();
		
		 for(int j=0;j<options.size();j++)
		 {
			 if(options.get(j).getText().equalsIgnoreCase(val))
				 return true;
		 }
		 
		 return false;
	}
	
	public boolean isElementPresent(By locator) throws Exception
	{	
		try
		{
			
			if(driver.findElement(locator) != null)
				return true;
			else 
				return false;
		}
		catch(NoSuchElementException ne)
		{
			logger.debug("Element with locator" + locator +" doesn't exist in the page");
			return false;
		
		}
	}
	
	public boolean isDisplayed(By locator) throws Exception
	{	
		try
		{
			
			if(isElementPresent(locator))
			{
				if(driver.findElement(locator).isDisplayed())
					return true;
				else
					return false;
			}
			else 
				return false;
		}
		catch(NoSuchElementException ne)
		{
			logger.debug("Element with locator" + locator +" is not displayed in the page");
			return false;
		
		}
	}
	public boolean isTextPresent(String text) throws Exception
	{
		return driver.findElement(By.tagName("body")).getText().contains(text);
	}
	
	public WebElement getWebElement(By locator)
	{
		return driver.findElement(locator);
	}
	
	public Select getSelect(By locator)
	{
		return new Select(driver.findElement(locator));
	}
	
	public void mouseMove(By locator)
	{
		 Actions builder = new Actions(driver);
		 Action move=builder.moveToElement(getWebElement(locator)).build();
		 move.perform();
	}
	
	
	public void mouseMove(By locator,int x,int y)
	{
		 Actions builder = new Actions(driver);
		 Action move=builder.moveToElement(getWebElement(locator),x,y).build();
		 move.perform();
	}
	
	/**
	 *  returns webElement object whose locator is locator.. type is type of locator id or name or xpath or link
	 * @param type
	 * @param locator
	 * @return
	 */
	public WebElement getElement(String type,String locator)
	{
		if(type.contains("id"))
			return getWebElement(By.id(locator));
		else if(type.contains("name"))
			return getWebElement(By.name(locator));
		else if(type.contains("link"))
			return getWebElement(By.linkText(locator));
		else
			return getWebElement(By.xpath(locator)); 
				
	}
}
