package com.ril.lib;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Rajesh Vemula
 *
 */
public class utils {
	public static final Logger logger = LoggerFactory.getLogger(utils.class);
	private static PropertiesConfiguration config;

	public static void setConfiguration(PropertiesConfiguration aConfig) {
		config = aConfig;
	}

	public static void setConfiguration(String file) throws ConfigurationException {
		config = new PropertiesConfiguration(file);
	}

	public static String getConfig(String configKey) {
		return config.getString(configKey);
	}

	public static String getConfig(String configKey,String defaultValue) {
		return config.getString(configKey,defaultValue);
	}
	public static String[] getConfigAsStringArray(String configKey) {
		return config.getStringArray(configKey);
	}

	public static void setConfig(String configKey, String configValue) {
		config.setProperty(configKey, configValue);
	}


}
