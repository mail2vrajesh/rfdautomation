package com.ril.lib;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;


/**
 * @author Rajesh Vemula
 *
 */
public class ReadExcel_v1 {
	public static final Logger logger = LoggerFactory.getLogger(ReadExcel_v1.class);
	private Sheet dataSheet=null;
	
	public ReadExcel_v1(String xlsfile) throws IndexOutOfBoundsException, BiffException, IOException {
		// TODO Auto-generated constructor stub
		this(xlsfile,1);
	}

	public ReadExcel_v1(String xlsfile,String sheetName) throws IndexOutOfBoundsException, BiffException, IOException {
		// TODO Auto-generated constructor stub
		
		dataSheet=Workbook.getWorkbook(new File(xlsfile)).getSheet(sheetName);
		
		}

	/**
	 * Sheet number starts with 1
	 * @param xlsfile
	 * @param sheetNumber
	 * @throws IndexOutOfBoundsException
	 * @throws BiffException
	 * @throws IOException
	 */
	public ReadExcel_v1(String xlsfile,int sheetNumber) throws IndexOutOfBoundsException, BiffException, IOException {
		// TODO Auto-generated constructor stub
	
		dataSheet=Workbook.getWorkbook(new File(xlsfile)).getSheet(sheetNumber-1);
		
	}
/**
 *  Returns the column data which row contains 'RowValue'
 *  @author Rajesh.Vemula
 * @param RowValue
 * @param ColumnName
 * @return
 * @throws BiffException
 * @throws IOException
 */
 	public String getTestData(String RowValue,String ColumnName) throws BiffException, IOException
	{
 		try
 		{
		
			Cell c1=dataSheet.findCell(RowValue);
			
			if (c1== null)
				return "";
			else
			{
				int fndRow=c1.getRow();
				int fndCol=dataSheet.findCell(ColumnName, 0, 0, 200, 0, false).getColumn();
				return dataSheet.getCell(fndCol, fndRow).getContents();
				
			}
 		}
 		catch(NullPointerException e)
 		{
 			throw new NullPointerException("There is no column with the column name as"+ColumnName);
 		}

	}
				
	public HashMap<String, String> getRowData(String RowValue) throws Exception
	{
		Cell c1=dataSheet.findCell(RowValue);
		
		if (c1== null)
			return null;
		else
		{
			Cell columns[]=dataSheet.getRow(0);
			Cell Values[]=dataSheet.getRow(c1.getRow());
			HashMap<String, String> hm = new HashMap<String, String>();
			
			for(int i=0;i<Values.length;i++)
			{
				hm.put(columns[i].getContents(), Values[i].getContents());
			}
			
			return hm;
		}
	}
    
	 public HashMap<String, String>[][] getTableToHashMapDoubleArray(int noOfRows) throws Exception{
			HashMap<String, String>[][] tabArray=null;
			HashMap<String, String>[] tabData=null;
	        try{
	            tabArray=new HashMap[noOfRows-1][1];
	            tabData=getTableToHashMapArray(noOfRows);
	            for (int i=0;i<noOfRows;i++)
	            {
	            	tabArray[i][0] = tabData[i];
	            }
	        }
	        catch (Exception e)    {
	        	e.printStackTrace();
	            System.out.println("error in getTableToHashMap()" + e.getMessage());
	            throw new Exception("Error is parsing Excel file" + e.getMessage());
	        }

	        return(tabArray);
	    }

	 public HashMap[][] getTableToHashMapDoubleArray() throws Exception{
	        try{
	            
	            return getTableToHashMapDoubleArray(dataSheet.getRows());
	        }
	        catch (Exception e)    {
	        	e.printStackTrace();
	            System.out.println("error in getTableToHashMap()" + e.getMessage());
	            throw new Exception("Error is parsing Excel file" + e.getMessage());
	        }


	    }


	 public HashMap<String, String>[] getTableToHashMapArray(int noOfRows) throws Exception{
			HashMap<String, String>[] tabArray=null;
	        
	        try{
	            int  endCol,ci,cj;
	            
	            endCol=dataSheet.getColumns();
	            tabArray=new HashMap[noOfRows-1];
	            ci=0;
	            for (int i=1;i<noOfRows;i++,ci++){
	            	HashMap<String, String> hm=new HashMap<String, String>();
	                cj=0;
	                for (int j=0;j<endCol;j++,cj++){
	                	hm.put(dataSheet.getCell(j,0).getContents(), dataSheet.getCell(j,i).getContents());
	                    //tabArray[ci][cj]=sheet.getCell(j,i).getContents();
	                }
	                tabArray[ci] = new HashMap<String, String>();
	                tabArray[ci].putAll(hm);
	            }
	        }
	        catch (Exception e)    {
	        	e.printStackTrace();
	            System.out.println("error in getTableToHashMap()" + e.getMessage());
	            throw new Exception("Error is parsing Excel file" + e.getMessage());
	        }

	        return(tabArray);
	    }

	 public HashMap[]getTableToHashMapArray() throws Exception{
	        try{
	            
	            return getTableToHashMapArray(dataSheet.getRows());
	        }
	        catch (Exception e)    {
	        	e.printStackTrace();
	            System.out.println("error in getTableToHashMap()" + e.getMessage());
	            throw new Exception("Error is parsing Excel file" + e.getMessage());
	        }


	    }

	 /**
	  *  rowNumber starts with 1
	  * @param rowNumber
	  * @return
	  * @throws Exception
	  */
	 public HashMap getRowData(int rowNumber) throws Exception{
	     HashMap<String, String> hm=new HashMap<String, String>();   
		 try{
	        	int endCol=dataSheet.getColumns();
	            
	            for (int j=0;j<endCol;j++){
                	hm.put(dataSheet.getCell(j,0).getContents(), dataSheet.getCell(j,rowNumber-1).getContents());
                
                }
	            return hm;
	        }
	        catch (Exception e)    {
	        	e.printStackTrace();
	            System.out.println("error in getTableToHashMap()" + e.getMessage());
	            throw new Exception("Error is parsing Excel file" + e.getMessage());
	        }


	    }

	
	 public HashMap<String,HashMap<String, String>> putWholeTableToHashMap() throws Exception
	 {

		HashMap<String,HashMap<String, String>> tabArray=new HashMap<String, HashMap<String,String>>();
		
	        try{
	            int  endCol;
	            int noOfRows=dataSheet.getRows();
	            endCol=dataSheet.getColumns();
	            
	            for (int i=1;i<noOfRows;i++){
	            	HashMap<String, String> hm=new HashMap<String, String>();

	                for (int j=0;j<endCol;j++){
	                	hm.put(dataSheet.getCell(j,0).getContents(), dataSheet.getCell(j,i).getContents());
	                }
	                tabArray.put(dataSheet.getCell(0,i).getContents(),hm);
	                //hm.
	                
	            }
	        }
	        catch (Exception e)    {
	        	e.printStackTrace();
	            System.out.println("error in getTableToHashMap()" + e.getMessage());
	            throw new Exception("Error is parsing Excel file" + e.getMessage());
	        }

	        return tabArray;	    
	 }
	 
		public String[][] getTableArray() throws Exception{
	        try{
	            return getTableArray(dataSheet.getRows());
	            }
	        catch (Exception e)    {
	        	e.printStackTrace();
	            System.out.println("error in getTableArray()" + e.getMessage());
	            throw new Exception("Error is parsing Excel file" + e.getMessage());
	        }
	    }

		public String[][] getTableArray(int noOfRowstoProcess) throws Exception{
	        String[][] tabArray=null;
	        try{
	            
	            int endRow, endCol,ci,cj;
	            endRow= dataSheet.getRows();
	            endCol=dataSheet.getColumns();
	            
	            tabArray=new String[noOfRowstoProcess-1][endCol];
	            ci=0;

	            for (int i=1;i<endRow;i++,ci++){
	                cj=0;
	                for (int j=0;j<endCol;j++,cj++){
	                    tabArray[ci][cj]=dataSheet.getCell(j,i).getContents();
	                }
	            }
	        }
	        catch (Exception e)    {
	        	e.printStackTrace();
	            System.out.println("error in getTableArray()" + e.getMessage());
	            throw new Exception("Error is parsing Excel file" + e.getMessage());
	        }

	        return(tabArray);
	    }


}
