package com.ril.lib;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Rajesh Vemula
 *
 */
public class WaitEvent {

	public static final Logger logger = LoggerFactory.getLogger(WaitEvent.class);

	private int DEFAULT_TIMEOUT_SEC = 10;
	private int DEFAULT_POLL_SEC = 1;
	
	private WebDriver driver = null;
	private By locator = null;
	private int timeOutInSeconds;
	private int pollIntervalInSeconds;
	private String text=null;
	private WebElement element=null;
	
	public WaitEvent(WebDriver driver, int duration_in_sec, int poll_interval_in_sec) {
		this.driver = driver;
		this.timeOutInSeconds = duration_in_sec;
		this.pollIntervalInSeconds = poll_interval_in_sec;
	}

	public WaitEvent(WebDriver driver) {
		this.driver = driver;
		this.timeOutInSeconds = DEFAULT_TIMEOUT_SEC;
		this.pollIntervalInSeconds = DEFAULT_POLL_SEC;
	}
	public WaitEvent(WebDriver driver, int duration_in_sec) {
		this.driver = driver;
		this.timeOutInSeconds = duration_in_sec;
		this.pollIntervalInSeconds = DEFAULT_POLL_SEC;
	}

	public void setTimeOut(int duration_in_sec)
	{
		this.timeOutInSeconds = duration_in_sec;
	}
	public void setPollingInterval(int poll_interval_in_sec)
	{
		this.pollIntervalInSeconds = poll_interval_in_sec;
	}
	public void changeDriver(WebDriver driver)
	{
		this.driver = driver;
	}
	
	public WebElement waitForElement(By locat, int durationSecs) throws Exception {
		try
		{
			this.locator=locat;
			this.timeOutInSeconds=durationSecs;
			
			logger.debug("Waiting for element with locator '" + this.locator.toString() + "' ");
			Wait<WebDriver> wait = new WebDriverWait(driver, this.timeOutInSeconds).pollingEvery(this.pollIntervalInSeconds, TimeUnit.SECONDS);
			try
			{
				wait.until(new ExpectedCondition<Boolean>() {
		            public Boolean apply(WebDriver webDriver) {
		                return webDriver.findElement(locator).isDisplayed();
		            }
		        });
				logger.debug("Found.");
				WebElement elem = driver.findElement(locator);
				return elem;
			}
			catch(StaleElementReferenceException ste)
			{
				logger.info("Stale element exception occured"+ste.getMessage());
				Thread.sleep(2000);
				wait.until(new ExpectedCondition<Boolean>() {
		            public Boolean apply(WebDriver webDriver) {
		                return webDriver.findElement(locator).isDisplayed();
		            }
		        });
				logger.debug("Found.");
				WebElement elem = driver.findElement(locator);
				return elem;
			}
		}
		catch(TimeoutException te)
		{
			logger.info(locat + " Element didn't apear in the specified time");
			return null;
		}
	}
	
	public WebElement waitForElement(By locat) throws Exception {
		
			return waitForElement(locat, timeOutInSeconds);
	}
	
	public WebElement waitForElement(WebElement elmnt) throws Exception {
		try
		{
			this.element=elmnt;
			logger.debug("Waiting for element with locator '" + this.locator.toString() + "' ");
			Wait<WebDriver> wait = new WebDriverWait(driver, this.timeOutInSeconds).pollingEvery(this.pollIntervalInSeconds, TimeUnit.SECONDS);
			wait.until(new ExpectedCondition<Boolean>() {
	            public Boolean apply(WebDriver webDriver) {
	            	return element.isDisplayed();
	            }
	        });
			logger.debug("Found.");
			WebElement elem = driver.findElement(locator);
			return elem;
		}
		catch(TimeoutException te)
		{
			logger.info(elmnt + " Element didn't apear in the specified time");
			return null;
		}
	}
	
	public boolean waitForText(String txt) throws Exception {
		try
		{
			text=txt;
			logger.debug("Waiting for text: '" + this.text + "' to appear on the page.");
			Wait<WebDriver> wait = new WebDriverWait(driver, this.timeOutInSeconds).pollingEvery(this.pollIntervalInSeconds, TimeUnit.SECONDS);
			wait.until(new ExpectedCondition<Boolean>() {
	            public Boolean apply(WebDriver webDriver) {
	            	try {
	            		return (webDriver.findElement(By.tagName("body")).getText().contains(text));
	            	} catch (StaleElementReferenceException e) {
	            		return false;
	            	} catch (NoSuchElementException e) {
	            		return false;
	            	}
	            }
	        });
			logger.debug("Found.");
			return true;
		}
		catch(TimeoutException te)
		{
			logger.info(txt + " text didn't apear in the specified time");
			return false;
		}
	}
	public boolean waitForText(String txt, int durationSecs) throws Exception {
		this.timeOutInSeconds=durationSecs;
		return waitForText(txt);
	}
	
	public boolean waitForTextDisappear(String txt) throws Exception {
		text=txt;
		logger.debug("Waiting for text: '" + this.text + "' to disappear from the page.");
		Wait<WebDriver> wait = new WebDriverWait(driver, this.timeOutInSeconds).pollingEvery(this.pollIntervalInSeconds, TimeUnit.SECONDS);
		wait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                return (webDriver.findElement(By.tagName("body")).getText().contains(text) == false);
            }
        });
		logger.debug("Gone.");
		return true;
	}
	
	public boolean waitForTextDisappear(String txt,int durationSecs) throws Exception {
		timeOutInSeconds=durationSecs;
		return waitForTextDisappear(txt);
	}
	
	public boolean waitForElementDisappear(By locat) throws Exception {
		this.locator=locat;
		logger.debug("Waiting for element with ID '" + this.locator.toString() + "' to disappear.");
		Wait<WebDriver> wait = new WebDriverWait(driver, this.timeOutInSeconds).pollingEvery(this.pollIntervalInSeconds, TimeUnit.SECONDS);
		wait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
            	try {
            		return (webDriver.findElement(locator).isDisplayed() == false);
            	} catch(NoSuchElementException e) {
            		return true;
            	}
            }
        });
		logger.debug("Gone.");
		return true;
	}
	public boolean waitForElementDisappear(By locat,int durationSecs) throws Exception {
		timeOutInSeconds=durationSecs;
		return waitForElementDisappear(locat);
	}

	public boolean waitForPageTitle(String title) throws Exception {
		try
		{
			text=title;
			logger.debug("Waiting for Page Title: '" + this.text + "'");
			Wait<WebDriver> wait = new WebDriverWait(driver, this.timeOutInSeconds).pollingEvery(this.pollIntervalInSeconds, TimeUnit.SECONDS);
			wait.until(new ExpectedCondition<Boolean>() {
	            public Boolean apply(WebDriver webDriver) {
	            	try {
	            		return (webDriver.getTitle().contains(text));
	            	} catch (StaleElementReferenceException e) {
	            		return false;
	            	} catch (NoSuchElementException e) {
	            		return false;
	            	}
	            }
	        });
			logger.debug("Page Title appeared as" + title);
			return true;
		}
		catch(TimeoutException te)
		{
			logger.info(title + " Title for the page didn't apear in the specified time");
			return false;
		}
	}

	public boolean waitForPageTitle(String title, int durationSecs) throws Exception {
		timeOutInSeconds=durationSecs;
		return waitForPageTitle(title);
	}

	public boolean waitForTextForElement(By locatorOfElement,String txt)
	{
		try
		{
			text=txt;
			this.locator=locatorOfElement;
			logger.debug("Waiting for text: to appear for element" +locator);
			Wait<WebDriver> wait = new WebDriverWait(driver, this.timeOutInSeconds).pollingEvery(this.pollIntervalInSeconds, TimeUnit.SECONDS);
			wait.until(new ExpectedCondition<Boolean>() {
	            public Boolean apply(WebDriver webDriver) {
	            	try {
	            		return (webDriver.findElement(locator).getText().contains(text));
	            	} catch (StaleElementReferenceException e) {
	            		return false;
	            	} catch (NoSuchElementException e) {
	            		return false;
	            	}
	            }
	        });
			logger.debug("Found.");
			return true;
		}
		catch(TimeoutException te)
		{
			logger.info(txt + " text didn't apear in the specified time");
			return false;
		}
	}

	public boolean waitForTextForElement(By locatorOfElement,String txt,int durationSecs)
	{
		timeOutInSeconds=durationSecs;
		return waitForTextForElement(locatorOfElement, txt);
	}

}