package com.ril.lib;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;


/**
 * @author Rajesh Vemula
 *
 */
public class Validator {
	public static final Logger logger = LoggerFactory.getLogger(Validator.class);
	
	
	public static void verifyTextinPage(WebDriver driver, String text) throws Exception
	{
		verifyTextinPage(driver, text,0);
	}

	public static void verifyTextinPage(WebDriver driver, String text,int durationSecs) throws Exception
	{
		if(durationSecs > 0)
		{
			WaitEvent we=new WaitEvent(driver,durationSecs);
			we.waitForText(text);
			we=null;
		}
		logger.debug("Text to verify in the html body is " + text);
		
		if(!driver.findElement(By.tagName("body")).getText().contains(text))
		{	
			logger.error("Text " + text + " is not found in the page body of the page "+ driver.getTitle());
			Assert.fail("Text " + text + " is not found in the page body of the page "+ driver.getTitle());
		}
	}
	
	
	public static void verifyTitleOfPage(WebDriver driver, String title) throws Exception
	{
		verifyTitleOfPage(driver, title, 0);
	}
	
	public static void verifyTitleOfPage(WebDriver driver, String title,int durationSecs) throws Exception
	{
		if(durationSecs > 0)
		{
			WaitEvent we=new WaitEvent(driver,durationSecs);
			we.waitForPageTitle(title);
			we=null;
		}
		logger.debug("Verifying the page title" + title);
		if(!driver.getTitle().contains(title))
		{	
			logger.error("Page Title is not shown as " + title + "Actual Page Title is"+ driver.getTitle());
			
			Assert.fail("Page Title is not shown as " + title + "Actual Page Title is"+ driver.getTitle());
		}
		logger.debug("found succesfully");
	}
	
	public static boolean isElementPresent(WebDriver driver,By locator,int durationSecs) throws Exception
	{	
		try
		{
			if(durationSecs > 0)
			{
				WaitEvent we=new WaitEvent(driver,durationSecs);
				we.waitForElement(locator);
				we=null;
			}
			if(driver.findElement(locator) != null)
				return true;
			else 
				return false;
		}
		catch(NoSuchElementException ne)
		{
			logger.debug("Element with locator" + locator +" doesn't exist in the page");
			return false;
		
		}
	}

	public static boolean isElementPresent(WebDriver driver,By locator) throws Exception
	{	
		return isElementPresent(driver, locator,0);
	}

	
	public static boolean isTextPresent(WebDriver driver,String text,int durationSecs) throws Exception
	{
		if(durationSecs > 0)
		{
			WaitEvent we=new WaitEvent(driver,durationSecs);
			we.waitForText(text);
			we=null;
		}
		return driver.findElement(By.tagName("body")).getText().contains(text);
	}
	
	public static boolean isTextPresent(WebDriver driver,String text) throws Exception
	{
		return isTextPresent(driver, text,0);
	}
	public static void verifyLinkExistInPage(WebDriver driver, String linkText) throws Exception
	{
		verifyLinkExistInPage(driver, linkText, 0);
		
	}
	
	public static void verifyLinkExistInPage(WebDriver driver, String linkText,int durationSecs) throws Exception
	{
		if(durationSecs > 0)
		{
			WaitEvent we=new WaitEvent(driver,durationSecs);
			we.waitForElement(By.linkText(linkText));
			we=null;
		}
		logger.debug("Verifying for link with text as" + linkText );
		
		if(!isElementPresent(driver,By.linkText(linkText)) )
		{	
			logger.error("There is no link in the page with text as " + linkText);
			
			Assert.fail("There is no link in the page with text as " + linkText);
		}
		logger.debug("found succesfully");
	}

	public static void verifyElementExistInPage(WebDriver driver, By locator) throws Exception
	{
		verifyElementExistInPage(driver, locator, 0);
	}
	
	public static void verifyElementExistInPage(WebDriver driver, By locator,int durationSecs) throws Exception
	{
		if(durationSecs > 0)
		{
			WaitEvent we=new WaitEvent(driver,durationSecs);
			we.waitForElement(locator);
			we=null;
		}
		logger.debug("Verifying for locator" + locator );
		
		if(!isElementPresent(driver, locator) )
		{	
			logger.error("The webelement " + locator +"doesn't exist in the page");
			Assert.fail("The webelement " + locator +"doesn't exist in the page");
		}
		logger.debug("found succesfully");
		
	}

	public static void verifyTextNotInPage(WebDriver driver, String text) throws Exception
	{
		verifyTextNotInPage(driver, text, 0);
	}
	
	public static void verifyTextNotInPage(WebDriver driver, String text,int durationSecs) throws Exception
	{
		if(durationSecs > 0)
		{
			WaitEvent we=new WaitEvent(driver,durationSecs);
			we.waitForTextDisappear(text);
			we=null;
		}
		logger.debug("verifying for text for not to exist in the html body is " + text);
		
		if(driver.findElement(By.tagName("body")).getText().contains(text))
		{	
			logger.error("Text " + text + " is found in the page body of the page "+ driver.getTitle());
			Assert.fail("Text " + text + " is found in the page body of the page "+ driver.getTitle());
		}
		
		logger.debug("Not found succesfully");
	}
	

	public static void verifyTextForElement(WebDriver driver, By locator, String text,int durationSecs) throws Exception
	{
		if(durationSecs > 0)
		{
			WaitEvent we=new WaitEvent(driver,durationSecs);
			we.waitForTextForElement(locator,text,durationSecs);
			we=null;
		}
		logger.debug("Text to verify in the html body is " + text);
		
		if(!driver.findElement(locator).getText().contains(text))
		{	
			logger.error("Text " + text + " is not found for Element "+ locator);
			Assert.fail("Text " + text + " is not found for Element "+ locator);
		}
	}
}
