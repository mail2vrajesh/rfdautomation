package com.ril.lib;

import java.io.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * @author Rajesh Vemula
 *
 */
public class UIMapParser {

	public static final Logger logger = LoggerFactory.getLogger(UIMapParser.class);
	private File repositoryXml=null;

	private PropertiesConfiguration config=null;
	private Sheet repoSheet=null;
	/**	
	 *  Default constructor and UIobj.xml located at resources folder is taken as Repository
	 */
	// TODO Auto-generated constructor stub
	public UIMapParser() {

		repositoryXml=new File("src\\test\\resources\\UIobj.xml");

	}
	/***
	 *  Constructor with repository location
	 * @param file
	 * @throws IndexOutOfBoundsException
	 * @throws BiffException
	 * @throws IOException
	 * @throws ConfigurationException
	 */
	public UIMapParser(String file) throws IndexOutOfBoundsException, BiffException, IOException, ConfigurationException {

		if(file.contains("xml"))
			setRepositoryAsXML(file);
		else if(file.contains("xls"))
			setRepositoryAsXLS(file);
		else if(file.contains("properties"))
			setRepositoryAsPropertiesFile(file);
		
	}

	public void setRepository(String file) throws Exception {

		if(file.contains("xml"))
			setRepositoryAsXML(file);
		else if(file.contains("xls"))
			setRepositoryAsXLS(file);
		else if(file.contains("properties"))
			setRepositoryAsPropertiesFile(file);
		
	}

	/**
	 * Provide UI object xml file as input for furhter processing
	 * @param xmlFile
	 */
	
	public void setRepositoryAsXML(String xmlFile)
	{
		repositoryXml=new File(xmlFile);
		config=null;
		repoSheet=null;
	}
	
	
	public void setRepositoryAsXLS(String xlsFile) throws IndexOutOfBoundsException, BiffException, IOException
	{
		//repositoryXls=new File(xlsFile);
		repoSheet=Workbook.getWorkbook(new File(xlsFile)).getSheet(0);
		repositoryXml=null;
		config=null;
	}
	
	public void setRepositoryAsPropertiesFile(String propFile) throws ConfigurationException
	{
		config= new PropertiesConfiguration(propFile);
		repositoryXml=null;
		repoSheet=null;
	}
	
	
	
	/*********************************************************************************************
	 * public static Document getXMLPage
	 * Returns a DOM corresponding to File passed 
	 * 
	 * @param fileToParse
	 * @return
	 * @throws Exception
	 *********************************************************************************************/
	public Document getXMLPage(File fileToParse) throws Exception 
	{
		Document document = null;
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = builderFactory.newDocumentBuilder();
		document = builder.parse(fileToParse);
		return document;
	}

    /*********************************************************************************************
     * getTarget(String pageName ,String uiElementName)
     * returns translated 'target' element set by repositoryXml and element uiobject
     * 
     * @param pageName
     * @param uiElementName
     * @return
     * @throws Exception
    **********************************************************************************************/
    private String getElementValue(String pageName,String uiElementName) throws Exception
    {
    	Document dom = getXMLPage(repositoryXml);
    	Element racine = dom.getDocumentElement();
    	
    	NodeList pages = racine.getElementsByTagName("page");
    	//System.out.println(pages.getLength());
    	for (int i=0; i<pages.getLength();i++)
    	{
    		Element e = (Element) pages.item(i);
    		String pageValue = e.getAttribute("name");
    		//System.out.println("\nPage = " + pageValue);
    		if(pageValue.equalsIgnoreCase(pageName))
    		{
    			//System.out.println("\ninsidethis");
    		NodeList uiElements = e.getElementsByTagName("uiobject");
    		for (int j=0; j<uiElements.getLength();j++)
        	{
        		
    		Element ui_e = (Element) uiElements.item(j);
    		
    		String UIObjectValue = ui_e.getAttribute("name");
    		//System.out.println("\n UI ObjectName = " + UIObjectValue);
    		Node targetNode = ui_e.getElementsByTagName("locator").item(0);
    		String targetValue = targetNode.getTextContent();
    		//System.out.println("target value = " + targetValue);
    		
    		if (uiElementName.compareTo(UIObjectValue)==0)
    		{
    		return targetValue.trim();
    		}
        	}
        	}
    	}
    	return null;
    }
    
   
    private By getElmLocator(String sElmnt)
    {
    	if(sElmnt==null)
    		return null;
    	else
    	{
    		
	    	if(sElmnt.trim().startsWith("id="))
				return By.id(sElmnt.split("id=")[1]);
			else if(sElmnt.trim().startsWith("name="))
				return By.name(sElmnt.split("name=")[1]);
			else if(sElmnt.trim().startsWith("xpath="))
				return By.xpath(sElmnt.substring(6).trim());
			else if(sElmnt.trim().startsWith("link="))
				return By.linkText(sElmnt.split("link=")[1]);
	 		else if(sElmnt.trim().startsWith("classname="))
				return By.className(sElmnt.split("classname=")[1]);
	 		else if(sElmnt.trim().startsWith("tagname="))
				return By.tagName(sElmnt.split("tagname=")[1]);
	 		else
	 			return By.xpath(sElmnt);
    	}	
    }
    /**
     * @author Rajesh.Vemula
     *  returns Locator for the pagename and element that is provided
     */
    public By getLocator(String pageName,String uiElementName) throws Exception
    {
    	if(repositoryXml!=null)
    		return getElmLocator(getElementValue(pageName,uiElementName));
    	else if(repoSheet!=null)
    		return getElmLocator(getElementValuefromXls(pageName,uiElementName));
    	else
    		return getElmLocator(getElementValuefromProperties(pageName,uiElementName));
    }

    
    
    /**
     *  Excel Functions
     */
 	public String getElementValuefromXls(String pageName,String objectname) throws BiffException, IOException
	{
		
		Cell c1=repoSheet.findCell(objectname);
		
		if (c1== null)
			return "";
		else
		{
			int fndRow=c1.getRow();
			if(pageName.equalsIgnoreCase(repoSheet.getCell(0,fndRow).getContents()) ) 
				return repoSheet.getCell(2,fndRow).getContents();
			else
			{
				Cell c2=repoSheet.findCell(pageName);
				//int strtRow=c1.getRow();
				for(int strtRow=c2.getRow();strtRow<repoSheet.getRows();strtRow++)
				{
					if(objectname.equalsIgnoreCase(repoSheet.getCell(1,strtRow).getContents()) && pageName.equalsIgnoreCase(repoSheet.getCell(0,strtRow).getContents())) 
						return repoSheet.getCell(2,strtRow).getContents();
				}
				return "";
			}
			
		}

	}
				
	public String getElementValuefromProperties(String pageName,String objectname) throws BiffException, IOException
	{
		
		return config.getString(pageName+"."+objectname);

	}

    public void test()
    {}
}
