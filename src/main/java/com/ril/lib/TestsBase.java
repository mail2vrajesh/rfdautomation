package com.ril.lib;

import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeDriverService.Builder;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

/**
 * @author Rajesh Vemula
 * 
 */
public class TestsBase extends SelBase {
	public static final Logger logger = LoggerFactory
			.getLogger(TestsBase.class);
	protected UIMapParser ui;
	protected static String remoteServer = null;
	protected static String remotePort = null;
	private ChromeDriverService service;

	@Parameters({ "config-file", "RemoteServer", "RemotePort" })
	@BeforeSuite(alwaysRun = true)
	public void testInit(@Optional String confFile,
			@Optional String RemoteServer, @Optional String RemotePort)
			throws Exception {
		logger.info("**************** Starting the suite ****************");

		if (confFile == null) {
			// Read config file
			utils.setConfiguration("src/test/resources/config.properties");
		} else
			utils.setConfiguration(confFile);
		remoteServer = RemoteServer;
		if (remoteServer != null)
			logger.info("Remote server to be used for execution" + remoteServer);
		else
			logger.debug("Remote server value is null");

		remotePort = RemotePort;
		// Instantiate UI map parser
		setupUIMapParser();
	}

	@BeforeClass(alwaysRun = true)
	public void initClass() throws Exception {
		logger.info("**************** Starting the suite ****************");
		// Instantiate UI map parser
		setupUIMapParser();
	}

	@AfterSuite(alwaysRun = true)
	public void testComplete() throws Exception {
		logger.info("**************** Starting the suite ****************");
	}

	// ------------------------------------------------------------------------
	// Pre-Test and Post-test methods
	// ------------------------------------------------------------------------
	// @Parameters({ "config-file" })
	@BeforeMethod(alwaysRun = true)
	public void testSetup(java.lang.reflect.Method method) throws Exception {
		logger.info("**************** Entering: " + method.getName()
				+ " ****************");

		// Remote server specified in config file gets priority
		// Start appropriate local/remote browser
		if (utils.getConfig("RemoteServer") != null) {
			// if(!(utils.getConfig("RemoteServer").contains("localhost")) )
			remoteServer = utils.getConfig("RemoteServer");
			if (utils.getConfig("RemotePort") != null)
				remotePort = utils.getConfig("RemotePort");
			else
				remotePort = "4444";
			startRemoteBrowser(remoteServer, remotePort);

		} else if (remoteServer != null) {
			if (remotePort == null)
				remotePort = "4444";

			startRemoteBrowser(remoteServer, remotePort);
		} else
			startLocalBrowser();

	}

	@AfterMethod(alwaysRun = true)
	public void testTeardown(java.lang.reflect.Method method, ITestResult result)
			throws Exception {

		try {
			if (result.isSuccess()) {
				logger.info("**************** PASSED: " + method.getName()
						+ " ****************");
				if (utils.getConfig("CaptureScreenshot") != null) {
					if (utils.getConfig("CaptureScreenshot").toUpperCase()
							.contains("ALL"))
						captureScreenshot(method.getName());
				}
			} else {
				logger.info("**************** FAILED: " + method.getName()
						+ " ****************");
				if (utils.getConfig("CaptureScreenshot") != null) {
					if (utils.getConfig("CaptureScreenshot").toUpperCase()
							.contains("ALL")
							|| utils.getConfig("CaptureScreenshot")
									.toUpperCase().contains("FAIL"))
						captureScreenshot(method.getName());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if ((utils.getConfig("Startmode") != null)
					&& utils.getConfig("RemoteServer") != null) {
				if (utils.getConfig("startmode").equalsIgnoreCase("Remote")
						&& (!(utils.getConfig("RemoteServer")
								.contains("localhost"))))
					this.closeRemoteBrowser();
			} else
				this.closeLocalBrowser();
		}
	}

	public void closeCurrentWindow() throws Exception {
		driver.close();
	}

	/**
	 * @author Rajesh.Vemula Starts remote browser
	 * @throws Exception
	 */
	private void startRemoteBrowser(String server, String port)
			throws Exception {
		// String port="4444";
		/*
		 * if(utils.getConfig("RemotePort")!=null)
		 * port=utils.getConfig("RemotePort");
		 */

		if (utils.getConfig("BROWSER").equals("firefox")) {
			// Create a profile which allows any SSL certificate error
			FirefoxProfile profile = new FirefoxProfile();
			profile.setAcceptUntrustedCertificates(true);
			profile.setAssumeUntrustedCertificateIssuer(false);

			DesiredCapabilities dc = DesiredCapabilities.firefox();
			dc.setCapability(FirefoxDriver.PROFILE, profile);
			this.driver = new RemoteWebDriver(new URL("http://" + server + ":"
					+ port + "/wd/hub"), dc);

		} else if (utils.getConfig("BROWSER").equals("chrome")) {

			DesiredCapabilities caps = DesiredCapabilities.chrome();
			caps.setCapability("chrome.switches", Arrays.asList(
					"--start-maximized", "--ignore-certificate-errors",
					"--disable-popup-blocking", "--disable-translate"));
			this.driver = new RemoteWebDriver(new URL("http://" + server + ":"
					+ port + "/wd/hub"), caps);

		} else if (utils.getConfig("BROWSER").equals("ie")) {
			DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
			this.driver = new RemoteWebDriver(new URL("http://" + server + ":"
					+ port + "/wd/hub"), caps);
			driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);

		} else if (utils.getConfig("BROWSER").equals("safari")) {
			// TODO

		} else {

			this.driver = new HtmlUnitDriver(true);

		}

		// instantiate super class waitevent object after driver is instantiated
		if (driver != null)
			we = new WaitEvent(driver);
		logger.info("Started local browser: " + utils.getConfig("Browser"));

	}

	/**
	 * @author Rajesh.Vemula Starts remote browser
	 * @throws Exception
	 */
	private void closeRemoteBrowser() throws Exception {

		this.driver.quit();
	}

	/**
	 * @author Rajesh.Vemula Starts the browser locally
	 * @throws Exception
	 */
	private void startLocalBrowser() throws Exception {

		if (utils.getConfig("BROWSER").equals("firefox")) {
			FirefoxProfile profile;
			
			if (getConfigVal("ffprofile").length() > 0) {
						profile = new FirefoxProfile(new File(
							utils.getConfig("ffprofile")));
			} else {
				// Create a profile which allows any SSL certificate error
				profile = new FirefoxProfile();
			}
			// Allow ssl certifcate errors
			profile.setAcceptUntrustedCertificates(true);
			profile.setAssumeUntrustedCertificateIssuer(true);
			
			//Changed for running without proxy - shankar kumar
			//*******************************************
			//Running with no proxy --- if needed
			//profile.setPreference("network.proxy.type", 0); 
			//***************************************************

			// Adding firefox to run fast
			if (getConfigVal("ffloadfast").length() > 0) {
					profile.setPreference("webdriver.load.strategy", "unstable");
			}

			// If using custom Firefox installation, we need to handle that
			String ffPath = getConfigVal("ffpth");
	
			if (ffPath.length() > 0) {
					FirefoxBinary bin = new FirefoxBinary(new File(ffPath));
					this.driver = new FirefoxDriver(bin, profile);
					logger.debug("Using custom firefox installation path: "
							+ ffPath);
				} else
					this.driver = new FirefoxDriver(profile);
		

		} else if (utils.getConfig("BROWSER").equals("chrome")) {
			if(getConfigVal("chromeservice").equalsIgnoreCase("no"))
			{
				
				 ChromeOptions options = new ChromeOptions();
				 options.addArguments("start-maximized");
				 options.addArguments("ignore-certificate-errors");
				 options.addArguments("disable-popup-blocking");
				 options.addArguments("disable-translate");
					  
				 System.setProperty("webdriver.chrome.driver", utils.getConfig("ChromeDriverPath"));
				 driver=new ChromeDriver(options);
				 
			}
			else
			{	
				// Note: List of all chrome command line switches is available at:
				// http://peter.sh/experiments/chromium-command-line-switches/
				if (utils.getConfig("ChromeDriverPath") != null) {
					service = ((Builder) new ChromeDriverService.Builder()).usingDriverExecutable(new File(utils.getConfig("ChromeDriverPath")))
							.usingAnyFreePort().build();
					service.start();
				} else {
					logger.error("Chrome Driver path is not specified in the config file. Specify the value in config.properties it using ChromeDriverPath=pathtodriver");
					System.out
							.println("Chrome Driver path is not specified in the config file. Specify the value in config.properties it using ChromeDriverPath=pathtodriver");
					throw new Exception(
							"Chrome Driver path is not specified in the config file. Specify the value in config.properties it using ChromeDriverPath=pathtodriver");
				}
				DesiredCapabilities caps = DesiredCapabilities.chrome();
				caps.setCapability("chrome.switches", Arrays.asList(
						"--start-maximized", "--ignore-certificate-errors",
						"--disable-popup-blocking", "--disable-translate"));
				this.driver = new RemoteWebDriver(service.getUrl(), caps);
	

			}
		} else if (utils.getConfig("BROWSER").equals("ie")) {
			// DesiredCapabilities caps =
			// DesiredCapabilities.internetExplorer();
			if (utils.getConfig("ieDriverPath") != null) { // caps.setCapability("webdriver.ie.driver",
															// utils.getConfig("ieDriverPath"));
				File file = new File(utils.getConfig("ieDriverPath"));
				System.setProperty("webdriver.ie.driver",
						file.getAbsolutePath());
			} else {
				logger.error("IE Driver path is not specified in the config file. Specify the value in config.properties it using ieDriverPath=pathtodriver");
				System.out
						.println("IE Driver path is not specified in the config file. Specify the value in config.properties it using ieDriverPath=pathtodriver");
				throw new Exception(
						"IE Driver path is not specified in the config file. Specify the value in config.properties it using ieDriverPath=pathtodriver");
			}
			DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
			caps.setCapability(
					CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION,
					true);
			this.driver = new InternetExplorerDriver(caps);
			driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
			/*
			 * this.driver = new RemoteWebDriver(new
			 * URL("http://localhost:4444/wd/hub"
			 * ),DesiredCapabilities.internetExplorer());
			 * driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
			 */
		} else if (utils.getConfig("BROWSER").equals("safari")) {
			// TODO
			/*DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setBrowserName("safari");
			CommandExecutor executor = new SeleneseCommandExecutor(new URL(
					"http://localhost:4444/"),
					new URL("http://www.yahoo.com/"), capabilities);
			this.driver = new RemoteWebDriver(executor, capabilities);
			this.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);*/
		} else {

			this.driver = new HtmlUnitDriver(true);

		}

		// instantiate super class waitevent object after driver is instantiated
		we = new WaitEvent(driver);
		logger.info("Started local browser: " + utils.getConfig("Browser"));
	}

	public String getBrowser() {
		return utils.getConfig("BROWSER");
	}

	private void closeLocalBrowser() {
		// Please use latest chrome driver see the issue below
		// http://code.google.com/p/chromedriver/issues/detail?id=99
		/*
		 * if (getBrowser().equals("chrome") ) this.driver.close(); else
		 */
		this.driver.quit();

		if (this.service != null) {
			this.service.stop();
		}
	}

	/**
	 * @author Rajesh.Vemula
	 * 
	 * @throws Exception
	 */

	public void setupUIMapParser() throws Exception {
		if (utils.getConfig("ObjectRepository") != null) {
			ui = new UIMapParser(utils.getConfig("ObjectRepository"));
		} else
			ui = new UIMapParser();
	}

	public boolean createFolder(String folder) {
		File f = new File(folder);
		try {
			if (f.isDirectory()) {
				logger.info(folder
						+ " Directory is not created as it already exists.");
				return false;
			}

			if (f.mkdir()) {
				logger.info(folder + " Directory has been Created");
				return true;
			} else {
				logger.info(folder + " Directory is not created");
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception raised while creating the folder" + folder);
			return false;
		}

	}

	// ------------------------------------------------------------------------
	// Private methods common to local/remote browsers
	// ------------------------------------------------------------------------
	private void captureScreenshot(String methodname) {
		if (driver.getClass().toString().contains("Html"))
			return;
		try {
			String sFolder = "target";
			if (utils.getConfig("ScreenshotFolder") != null) {
				if (!utils.getConfig("ScreenshotFolder").trim().equals(""))
					sFolder = utils.getConfig("ScreenshotFolder");
			}
			// Make a unique filename including current timestamp, browser and
			// method name.
			java.util.Date date = new java.util.Date();
			String parts[] = date.toString().split(" ");
			String fileName = sFolder + File.separator + "screenshots"
					+ File.separator + methodname + "_" + parts[5] + "_"
					+ parts[1] + "_" + parts[2] + "_" + parts[3].split(":")[0]+ "_"
					+parts[3].split(":")[1]+ "_"+ parts[3].split(":")[2]+ "_"
					+ utils.getConfig("BROWSER") + ".png";
			createFolder(sFolder + File.separator + "screenshots");
			takeScreenshot(fileName);
			logger.debug("Screenshot file name: " + fileName);

		} catch (Exception e) {
			// Ignore errors while taking the screenshot.
			logger.error("Failed to capture screenshot! Browser: "
					+ utils.getConfig("BROWSER"));
		}
	}

	/**
	 * @author Rajesh.Vemula To change the UI map repository
	 * @param newRepository
	 * @throws Exception
	 */
	public void setUIMapRepository(String newRepository) throws Exception {
		ui.setRepository(newRepository);
	}

	public By getLocator(String pageName, String uiElementName)
			throws Exception {
		return ui.getLocator(pageName, uiElementName);
	}

	public String getConfigVal(String val)
	{
		
		if(utils.getConfig(val)==null)
			return "";
		else
		{
			return utils.getConfig(val).toLowerCase();
		}	

	}
}
